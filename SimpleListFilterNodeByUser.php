<?php

require_once(drupal_get_path('module', 'simplelist') .'/SimpleListFilterParent.php');

class SimpleListFilterNodeByUser extends SimpleListFilterParent {
  
 /**
   * The main workhorse of the class, this function gets the list of node ids from the database, and then gets the loaded nodes out of the cache_engine.
   *
   * @param stdClass $simple_list
   *   SimpleList object from controller.
   * @param int $count
   *   The number of nodes to return.
   * @param int $offset
   *   The offset from the start - 0 means start at 1.  Basically, query starts at item 1+$offset and goes on to $count+$offset
   * @return array
   *   Array of loaded node objects.
   */
  public function get_node_list($simple_list, $count, $offset, $paged) {
    // reconstitute roles data from filter_data field:
    if ($simple_list->filter_data) {
      $filter_data = unserialize($simple_list->filter_data);
      if (!is_array($filter_data->roles)) {
        $roles = array();
        $uid = -1;
      }
      else {
        $roles = $filter_data->roles;
      }
      $uid = $filter_data->uid;
    }
    else {
      $roles = array();
      $uid = -1;
    }
    if (count($simple_list->node_types) == 0 && count($roles) == 0) { 
      return array();
    }
    $nodes = array();
    $query_args = array();
    $where_args = array();
    $query = '';
    $where = '';
    $order = '';
    
    if (count($simple_list->node_types) > 0) {
      $query = "SELECT n.nid FROM {node} n";
      $where = " WHERE n.type IN (". db_placeholders($simple_list->node_types, "varchar") .")";
      $where_args = $simple_list->node_types;
    }
    else {
      $query = "SELECT n.nid FROM {node} n";
    }
    
    if (count($roles)) {
      $query .= " INNER JOIN {users_roles} ur ON (n.uid = ur.uid AND ur.rid IN (". db_placeholders($roles) .")) ";
      //$where_args = array_merge($roles, $where_args);
      $query_args = $roles;
    }
    
    if ($uid > 0) {
      if ($where != '') {
        $where .= " AND";
      }
      else {
        $where .= " WHERE";
      }
      $where .= ' n.uid = %d';
      $where_args[] = $uid;
    }
    
    if ($simple_list->published == SIMPLELIST_PUBLISHED_NODES || $simple_list->published == SIMPLELIST_UNPUBLISHED_NODES) {
      if ($where != '') {
        $where .= " AND";
      }
      else {
        $where .= " WHERE";
      }
      $where .= " n.status = %d";
      $where_args[] = $simple_list->published;
    }
    
    $dir = $this->get_sort_order_from_sort_data($simple_list->sort_data);
    switch ($simple_list->sort_name) {
      case 'created':
        
        $order = ' ORDER BY n.created '. $dir;
        break;
      case 'title':
        $order = ' ORDER BY n.title '. $dir;
        break;
      case 'node_id':
        $order = ' ORDER BY n.nid '. $dir;
        break;
      case 'updated':
        $order = ' ORDER BY n.created '. $dir;
        break;
      case 'type':
        $order = ' ORDER BY n.type '. $dir .', created DESC';
        break;
      case 'comment_count':
        if (db_table_exists('node_comment_statistics')) {
          $query .= ' INNER JOIN {node_comment_statistics} ncs ON (ncs.nid = n.nid)';
          $order = ' ORDER BY ncs.comment_count '. $dir . ', n.created DESC';
        }
        break;
      case 'most_popular':
        if (db_table_exists('votingapi_cache')) {
          $query .= ' LEFT OUTER JOIN {votingapi_cache} vap ON (vap.content_id = n.nid AND vap.content_type = \'node\' AND vap.function = \'average\')';
          $order = ' ORDER BY vap.value '. $dir . ', n.created DESC';
        }
        break;
      case 'user_name':
        $query .= ' INNER JOIN {users} u ON (u.uid = n.uid)';
        $order = ' ORDER BY u.name '. $dir .', created DESC';
        break;
      default:
        $order = '';
        break;
    }
    
    if ($paged) {
      $result = pager_query(db_rewrite_sql($query . $where . $order), $count, 0, NULL, array_merge($query_args, $where_args));
    }
    else {
      $result = db_query_range(db_rewrite_sql($query . $where . $order), array_merge($query_args, $where_args), $offset, $count);
    }
    while ($node_id = db_fetch_object($result)) {
      $nodes[] = $this->cache_engine->fetch_node($node_id->nid);
    }
    
    return $nodes;
  }
  
  /**
   * Form for class parameters
   *
   * @param unknown_type $simplelist
   * @return unknown
   */
  public static function get_filter_form($simplelist) {
    $form = array();
    $nodes = array();
    $selected_terms = array();
    
    foreach (node_get_types() as $type => $info) {
      $nodes[$type] = $info->name;
    }
    
    $user_data = unserialize($simplelist->filter_data);
    $account = user_load($user_data->uid);
    
    $form['node_types'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Node Types'),
      '#default_value' => (isset($simplelist->node_types) ? $simplelist->node_types : array()),
      '#options' => $nodes,
      '#description' => t('Check each node type to display in the list.'),
      '#weight' => -6
    );
    
    //drupal_set_message(dprint_r(user_roles(), true));
    $form['user_roles'] = array(
      '#type' => 'checkboxes',
      '#title' => t('User Roles'),
      '#default_value' => $user_data->roles,
      '#options' => user_roles(),
      '#description' => t('Check each role whose nodes you want to display.'),
      '#weight' => -4,
    );
    
    $form['user_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Created by'),
      '#maxlength' => 60,
      '#autocomplete_path' => 'user/autocomplete',
      '#default_value' => $account->name,
      '#weight' => -2,
      '#description' => t('If you only want to see nodes by one user, enter that user name here.'),
    );
    
    $form_options = array(
        'created' => t('Date Created'),
        'updated' => t('Date Updated'),
        'title' => t('Title'),
        'node_id' => t('Node ID'),
        'user_name' => t('Author Name'),
        'type' => t('Node Type'),
        'comment_count' => t('Comment Count'),
     );
     
     if (db_table_exists('votingapi_cache')) {
       $form_options['most_popular'] = t('Most Popular');
     }
    
    $form['sort_name'] = array(
      '#type' => 'select',
      '#title' => t('Sort Order'),
      '#default_value' => $simplelist->sort_name,
      '#options' => $form_options,
      '#description' => 'The order to display nodes in.',
      '#weight' => 0,
    );
  
    $form['sort_data'] = array(
      '#type' => 'radios',
      '#title' => t('Sort Direction'),
      '#default_value' => $simplelist->sort_data,
      '#options' => array('ASC' => t('Ascending'), 'DESC' => t('Descending')),
      '#weight' => 2,
    );
    return $form;
  }
  
  public static function get_filter_form_validate(&$form, &$form_state) {
  
  }
  
  /**
   * Submit form for class' parameters.
   *
   * @param unknown_type $form_id
   * @param unknown_type $form_state
   */
  public static function get_filter_form_submit($form_id, &$form_state) {
    $old_simplelist = $form_state['values']['simplelist'];
    $node_types = array();
    foreach ($form_state['values']['node_types'] as $key => $value) {
      if ($value) {
        $node_types[] = $key;
      }
    }
    
    $delete_type_query = "DELETE FROM {simplelist_types} WHERE slid = %d AND node_type = '%s'";
    $insert_type_query = "INSERT INTO {simplelist_types} (slid, node_type) VALUES (%d, '%s')";
    $old_types = $old_simplelist->node_types;

    foreach ($old_types as $type) {
      if (($index = array_search($type, $node_types)) !== FALSE) {
        unset($node_types[$index]);
      }
      else {
        
        db_query($delete_type_query, $form_state['values']['slid'], $type);
      }
    }
    foreach ($node_types as $type) {
      db_query($insert_type_query, $form_state['values']['slid'], $type);
    }
    
    //$roles = serialize($form_state['values']['user_roles']);
    if ($form_state['values']['user_name']) {
      $account = user_load(array('name' => $form_state['values']['user_name']));
    }
    
    $roles = array();
    foreach ($form_state['values']['user_roles'] as $key => $data) {
      if ($data != 0) {
        $roles[] = $key;
      }
    }
    $user_settings = new stdClass();
    $user_settings->roles = $roles;
    $user_settings->uid = $account->uid;
    //dpm($user_settings);
    $user_data = serialize($user_settings);
    db_query("UPDATE {simplelist} SET filter_data = '%s' WHERE slid = %d", $user_data, $form_state['values']['slid']);
  }
  
  /**
   * Clean up old settings from this simplelist
   * 
   * Here we go through and clean up the settings specific to this filter for this simplelist.  This gets called by
   *   the form_submit if the user has switched from this filter to a different one, to make sure no leftover data
   *   is left behind.  We also clean out the old node_types data so that if the new filter uses it, then it's not 
   *   confused by thinking there's old data left behind that's not there.
   * 
   * This should also be called by contrib modules building on SimpleList on uninstall, so that data is deleted from
   *   lists which the contrib filter wrote.
   * 
   * @param unknown_type $slid
   * @param unknown_type $form_id
   * @param unknown_type $form_state
   */
  public static function clear_existing_settings($slid, $form_id='', &$form_state=NULL) {
    $delete_type_query = "DELETE FROM {simplelist_types} WHERE slid = %d";
    db_query($delete_type_query, $slid);
    if (is_array($form_state)) {
      if ($form_state['values']['simplelist']) {
        $form_state['values']['simplelist']->node_types = array();
      }
    }
    db_query("UPDATE {simplelist} SET filter_data = '' WHERE slid = %d", $slid);
  }
}