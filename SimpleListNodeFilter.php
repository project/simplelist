<?php

require_once(drupal_get_path('module', 'simplelist') .'/SimpleListFilterParent.php');

/*
 * This class filters nodes by type and/or taxonomy.
 */
class SimpleListNodeFilter extends SimpleListFilterParent {
  
  /**
   * The main workhorse of the class, this function gets the list of node ids from the database, and then gets the loaded nodes out of the cache_engine.
   *
   * @param stdClass $simple_list
   *   SimpleList object from controller.
   * @param int $count
   *   The number of nodes to return.
   * @param int $offset
   *   The offset from the start - 0 means start at 1.  Basically, query starts at item 1+$offset and goes on to $count+$offset
   * @return array
   *   Array of loaded node objects.
   */
  public function get_node_list($simple_list, $count, $offset, $paged) {
    if (count($simple_list->node_types) == 0 && count($simple_list->terms) == 0) { 
      return array();
    }
    $nodes = array();
    $query_args = array();
    $where_args = array();
    $query = '';
    $where = '';
    $order = '';
    
    if (count($simple_list->node_types) > 0 && count($simple_list->terms) > 0) {
      $query = "SELECT DISTINCT n.nid FROM {node} n INNER JOIN {term_node} tn ON (tn.tid IN (". db_placeholders($simple_list->terms) .") AND n.vid = tn.vid)";
      $where = " WHERE n.type IN (". db_placeholders($simple_list->node_types, "varchar") .")";
      foreach($simple_list->terms as $term) {
        $query_args[] = $term->tid;
      }
      /*foreach($simple_list->node_types as $type) {
        $where_args[] = $type;
      }*/
      $where_args = array_merge($where_args, $simple_list->node_types);
    }
    else if (count($simple_list->node_types) > 0) {
      $query = "SELECT n.nid FROM {node} n";
      $where = " WHERE n.type IN (". db_placeholders($simple_list->node_types, "varchar") .")";
      $where_args = $simple_list->node_types;
    }
    else {
      $query = "SELECT DISTINCT n.nid FROM {node} n INNER JOIN {term_node} tn ON (tn.tid IN (". db_placeholders($simple_list->terms) .") AND n.vid = tn.vid)";
      foreach($simple_list->terms as $term) {
        $query_args[] = $term->tid;
      }
    }
    
    if ($simple_list->published == SIMPLELIST_PUBLISHED_NODES || $simple_list->published == SIMPLELIST_UNPUBLISHED_NODES) {
      if ($where != '') {
        $where .= " AND";
      }
      else {
        $where .= " WHERE";
      }
      $where .= " n.status = %d";
      $where_args[] = $simple_list->published;
    }
    
    $dir = $this->get_sort_order_from_sort_data($simple_list->sort_data);
    switch ($simple_list->sort_name) {
      case 'created':
        
        $order = ' ORDER BY n.created '. $dir;
        break;
      case 'title':
        $order = ' ORDER BY n.title '. $dir;
        break;
      case 'node_id':
        $order = ' ORDER BY n.nid '. $dir;
        break;
      case 'updated':
        $order = ' ORDER BY n.changed '. $dir;
        break;
      case 'type':
        $order = ' ORDER BY n.type '. $dir .', created DESC';
        break;
      case 'comment_count':
        if (db_table_exists('node_comment_statistics')) {
          $query .= ' INNER JOIN {node_comment_statistics} ncs ON (ncs.nid = n.nid)';
          $order = ' ORDER BY ncs.comment_count '. $dir . ', n.created DESC';
        }
        break;
      case 'most_popular':
        if (db_table_exists('votingapi_cache')) {
          $query .= ' LEFT OUTER JOIN {votingapi_cache} vap ON (vap.content_id = n.nid AND vap.content_type = \'node\' AND vap.function = \'average\')';
          $order = ' ORDER BY vap.value '. $dir . ', n.created DESC';
        }
        break;
      case 'user_name':
        $query .= ' INNER JOIN {users} u ON (u.uid = n.uid)';
        $order = ' ORDER BY u.name '. $dir .', created DESC';
        break;
      default:
        $order = '';
        break;
    }
    
    if ($paged) {
      $result = pager_query(db_rewrite_sql($query . $where . $order), $count, 0, NULL, array_merge($query_args, $where_args));
    }
    else {
      $result = db_query_range(db_rewrite_sql($query . $where . $order), array_merge($query_args, $where_args), $offset, $count);
    }
    while ($node_id = db_fetch_object($result)) {
      $nodes[] = $this->cache_engine->fetch_node($node_id->nid);
    }
    
    return $nodes;
  }
  
  /**
   * This is the form for the class paramters.
   *
   * @param unknown_type $simplelist
   * @return unknown
   */
  public static function get_filter_form($simplelist) {
    $form = array();
    $nodes = array();
    $selected_terms = array();
    
    foreach (node_get_types() as $type => $info) {
      $nodes[$type] = $info->name;
    }
    
    $form['node_types'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Node Types'),
      '#default_value' => (isset($simplelist->node_types) ? $simplelist->node_types : array()),
      '#options' => $nodes,
      '#description' => t('Check each node type to display in the list.'),
      '#weight' => -6
    );
    
    $terms = array();
    $query = 'SELECT v.vid, v.name, td.tid, td.name AS term_name FROM {vocabulary} v INNER JOIN {term_data} td ON (v.vid = td.vid) ORDER BY v.weight, v.name, td.weight, td.name';
    $result = db_query(db_rewrite_sql($query, 'v', 'vid'));
    while ($term_row = db_fetch_object($result)) {
      $terms[$term_row->tid] = $term_row->name .': '. $term_row->term_name;
    }
    
    foreach($simplelist->terms as $term) {
      $selected_terms[] = $term->tid;
    } 
    
  
    $form['taxonomy_tids'] = array(
      '#type' => 'select',
      '#title' => t('Taxonomy terms'),
      '#default_value' => (isset($selected_terms) ? $selected_terms : array()),
      '#options' => $terms,
      '#multiple' => TRUE,
      '#size' => 8,
      '#description' => t('Select any taxonomy terms to restrict by.'),
      '#weight' => -4,
    );
    
    $form_options = array(
        'created' => t('Date Created'),
        'updated' => t('Date Updated'),
        'title' => t('Title'),
        'node_id' => t('Node ID'),
        'user_name' => t('Author Name'),
        'type' => t('Node Type'),
        'comment_count' => t('Comment Count'),
     );
     
     if (db_table_exists('votingapi_cache')) {
       $form_options['most_popular'] = t('Most Popular');
     }
    
    $form['sort_name'] = array(
      '#type' => 'select',
      '#title' => t('Sort Order'),
      '#default_value' => $simplelist->sort_name,
      '#options' => $form_options,
      '#description' => 'The order to display nodes in.',
      '#weight' => -2,
    );
  
    $form['sort_data'] = array(
      '#type' => 'radios',
      '#title' => t('Sort Direction'),
      '#default_value' => $simplelist->sort_data,
      '#options' => array('ASC' => t('Ascending'), 'DESC' => t('Descending')),
      '#weight' => 2,
    );
    return $form;
  }
  
  /**
   * This is the validation for the class parameters.
   *
   * @param unknown_type $form
   * @param unknown_type $form_state
   */
  public static function get_filter_form_validate(&$form, &$form_state) {
    
  }
  
  /**
   * This is the submit function for the class parameters.
   *
   * @param unknown_type $form_id
   * @param unknown_type $form_state
   */
  public static function get_filter_form_submit($form_id, &$form_state) {
    $old_simplelist = $form_state['values']['simplelist'];
    
    $node_types = array();
    foreach ($form_state['values']['node_types'] as $key => $value) {
      if ($value) {
        $node_types[] = $key;
      }
    }
    
    $delete_type_query = "DELETE FROM {simplelist_types} WHERE slid = %d AND node_type = '%s'";
    $insert_type_query = "INSERT INTO {simplelist_types} (slid, node_type) VALUES (%d, '%s')";
    $old_types = $old_simplelist->node_types;
    foreach ($old_types as $type) {
      if (($index = array_search($type, $node_types)) !== FALSE) {
        unset($node_types[$index]);
      }
      else {
        db_query($delete_type_query, $form_state['values']['slid'], $type);
      }
    }
    foreach ($node_types as $type) {
      db_query($insert_type_query, $form_state['values']['slid'], $type);
    }
    
    $delete_term_query = "DELETE FROM {simplelist_terms} WHERE slid = %d AND tid = %d";
    $insert_term_query = "INSERT INTO {simplelist_terms} (slid, tid) VALUES (%d, %d)";
    $old_terms = $old_simplelist->terms;
    foreach ($old_terms as $term) {
      if (isset($form_state['values']['taxonomy_tids'][$term->tid])) {
        unset($form_state['values']['taxonomy_tids'][$term->tid]);
      }
      else {
        db_query($delete_term_query, $form_state['values']['slid'], $term->tid);
      }
    }
    foreach ($form_state['values']['taxonomy_tids'] as $tid => $data) {
      db_query($insert_term_query, $form_state['values']['slid'], $tid);
    }
  }
  
  /**
   * Clean up old settings from this simplelist
   * 
   * Here we go through and clean up the settings specific to this filter for this simplelist.  This gets called by
   *   the form_submit if the user has switched from this filter to a different one, to make sure no leftover data
   *   is left behind.  We also clean out the old node_types data so that if the new filter uses it, then it's not 
   *   confused by thinking there's old data left behind that's not there.
   * 
   * This should also be called by contrib modules building on SimpleList on uninstall, so that data is deleted from
   *   lists which the contrib filter wrote.
   *
   * @param unknown_type $slid
   * @param unknown_type $form_id
   * @param unknown_type $form_state
   */
  public static function clear_existing_settings($slid, $form_id='', &$form_state=NULL) {
    $delete_term_query = "DELETE FROM {simplelist_terms} WHERE slid = %d";
    db_query($delete_term_query, $slid);
    if (is_array($form_state)) {
      if ($form_state['values']['simplelist']) {
        $form_state['values']['simplelist']->terms = array();
      }
    }
    
    $delete_type_query = "DELETE FROM {simplelist_types} WHERE slid = %d";
    db_query($delete_type_query, $slid);
    if (is_array($form_state)) {
      if ($form_state['values']['simplelist']) {
        $form_state['values']['simplelist']->node_types = array();
      }
    }
  }
}
?>