<?php

/**
 * This is the controller class - it's called by the simplelist.module to load data from the simplelist tables and call the various Filter and Display
 * classes as needed.
 *
 */
class SimpleListController {
  private $simplelist;
  
  /**
   * Generates a 'blank' simplelist object - used when creating a new one.
   *
   * @return stdClass
   *   Blank Simplelist
   */
  public static function getDefault() {
    $sl = new stdClass();
    
    $sl->slid = 0;
    $sl->name = '';
    $sl->description = '';
    $sl->access = array();
    $sl->filter_name = 'SimpleListNodeFilter';
    $sl->filter_data = '';
    $sl->sort_name = 'created';
    $sl->sort_data = 'DESC';
    $sl->cache = BLOCK_CACHE_PER_ROLE;
    $sl->published = SIMPLELIST_PUBLISHED_NODES;
    
    $sl->node_types = array();
    $sl->terms = array();
    
    $sl->displays = array();
    
    return $sl;
  }
  
  /**
   * Loads a simplelist from the database and stores it in a simplelist object.  It only gets the required data based on the current context.
   *
   * @param mixed $name
   *   Either the name of the simplelist, or the slid of it.  Either can be used to load.
   * @param string $context
   *   If 'block' or 'page', loads the simplelist with only those display options in the display field.
   *   If 'all', then we're modifying this list, and we load all of the display options in the displays field.
   * @return stdClass
   *   Simplelist.
   */
  public static function load_simple_list($name, $context) {
    if (is_numeric($name)) {
      $query = "SELECT slid, name, description, access, filter_name, filter_data, sort_name, sort_data, cache, published FROM {simplelist} WHERE slid = %d";
    }
    else {
      $query = "SELECT slid, name, description, access, filter_name, filter_data, sort_name, sort_data, cache, published FROM {simplelist} WHERE name = '%s'";
    }
    $result = db_query($query, $name);
    if ($temp_list = db_fetch_object($result)) {
      $temp_list->access = unserialize($temp_list->access);
      $temp_list->node_types = array(); // default
      $temp_list->terms = array(); // default
      
      $query = "SELECT node_type FROM {simplelist_types} WHERE slid = %d";
      $result = db_query($query, $temp_list->slid);
      while ($row = db_fetch_object($result)) {
        $temp_list->node_types[] = $row->node_type;
      }
      
      $query = "SELECT tid FROM {simplelist_terms} WHERE slid = %d";
      $result = db_query($query, $temp_list->slid);
      while ($row = db_fetch_object($result)) {
        $temp_list->terms[] = taxonomy_get_term($row->tid);
      }
      
      if ($context != 'all') {
        $query = "SELECT sldid, display_name, display_context, display_count, display_title, display_pager, display_more, display_format, display_path FROM {simplelist_display} WHERE slid = %d AND display_context = '%s'";
        $result = db_query($query, $temp_list->slid, $context);
        $temp_list->display = db_fetch_object($result);
        $temp_list->display->display_more_path = ''; // initialize.
        
        if ($context == 'block' && isset($temp_list->display->display_more) && $temp_list->display->display_more == 1) {
          $query = "SELECT display_path FROM {simplelist_display} WHERE slid = %d AND display_context = 'page'";
          $result = db_query($query, $temp_list->slid);
          
          if ($row = db_fetch_object($result)) {
            $temp_list->display->display_more_path = $row->display_path;
          }
        }
      }
      else {
        // context is all - we're editing or doing something to this simplelist.
        $temp_list->displays = array();
        $query = "SELECT sldid, display_name, display_context, display_count, display_title, display_pager, display_more, display_format, display_path FROM {simplelist_display} WHERE slid = %d";
        $result = db_query($query, $temp_list->slid, $context);
        while ($row = db_fetch_object($result)) {
          $temp_list->displays[$row->display_context] = $row;
        }
      }
      
      return $temp_list;
    }
    return NULL;
  }
  
  /**
   * Does a comparison between the role ids that are assigned to the simplelist->access field, and the roles that the current user has.
   * 
   * Note that if the simplelist's access list is empty, then this simply returns true.
   *
   * @param array $access
   *   array of role ids
   * @param array $roles
   *   array of role names
   * @return boolean
   */
  public static function checkSimpleListAccess($access, $roles) {
    if (!count($access)) {
      return TRUE;
    }
    $access_roles = array();
    $result = db_query("SELECT name FROM role WHERE rid IN (". db_placeholders($access) .")", $access);
    while($row = db_fetch_object($result)){
      $access_roles[] = $row->name;
    }
    
    return count(array_intersect($access_roles, $roles));
  }
  
  /**
   * Constructor of SimpleListController. 
   *
   * @param mixed $simplelist
   *   Either a simplelist that's already been constructed, or a name of a list.
   * @param string $context
   *   The context in which we're loading the list.
   */
  public function __construct($simplelist, $context) {
    if ($simplelist instanceof stdClass) {
      $this->simplelist = $simplelist;
    }
    else {
      $this->simplelist = SimpleListController::load_simple_list($simplelist, $context);
    }
  }
  
  /**
   * For testing - returns the current list.
   *
   * @return stdClass
   *   Simplelist
   */
  public function getSimpleList() {
    return $this->simplelist;
  }
  
  /**
   * The main workhorse of the process - calls the Filter and Display classes to generate the needed html.
   *
   * @param int $offset
   *   The offset to fetch the data by.
   * @return mixed
   *   Either an array for blocks, or HTML for pages.
   */
  public function getRenderedList($offset = 0) {
    global $user;
    if ($this->checkSimpleListAccess($this->simplelist->access, $user->roles)) {
      require_once(drupal_get_path('module', 'simplelist') .'/SimpleListCachingEngine.php');
      
      // Allow our users to interrupt our work.
      $this->callSimpleListHook('simplelist_pre_query');
      
      $paged = $this->simplelist->display->display_context == 'page' && $this->simplelist->display->display_pager == 1;
      
      //Get the nodes we need to display.
      $filter = $this->getFilter($this->simplelist->filter_name, new SimpleListCachingEngine());
      if ($filter != NULL) {
        $data_array = $filter->get_node_list($this->simplelist, $this->simplelist->display->display_count, $offset, $paged);
      }
      else {
        $data_array = array();
      }
      
      // Generate the html from the list of nodes.
      $display = $this->getDisplay($this->simplelist->display->display_name);
      if ($display != NULL) {
        $prefix = $this->callSimpleListHookReturnString('simplelist_pre_display');
        $data_rendered = $display->render($this->simplelist, $data_array);
        $postfix = $this->callSimpleListHookReturnString('simplelist_post_display');
        
        $data_rendered = $prefix . $data_rendered . $postfix;
      }
      
      // return the data in the format the context requires.
      if ($this->simplelist->display->display_context == 'block') {
        return array('subject' => $this->simplelist->display->display_title,
          'content' => $data_rendered);
      }
      else {
        return $data_rendered;
      }
    }
    return '';
  }
  
  /**
   * Allows other functions to impose arguments on the existing simplelist.
   * 
   */ 
  public function setArgument($name, $value) {
    if ($name == 'tid') {
      // set taxonomy argument.
      if (is_numeric($value)) {
        $term = taxonomy_get_term($value);
        if ($term) {
          $this->simplelist->terms = array($term);
        }
      }
      else {
        $termlist = explode(' ', $value);
        $termarray = array();
        foreach($termlist as $tid) {
          $term = taxonomy_get_term($tid);
          if ($term) {
            $termarray[] = $term;
          }
        }
        if (count($termarray)) {
          $this->simplelist->terms = $termarray;
        }
      }
      return;
    }
    
    if ($name == 'node_type') {
      // Set type argument
      $this->simplelist->node_types = array($value->type);
    }
  }
  
  /**
   * Dynamically loads the Filter, making sure that it can be loaded and that the loaded class is descended from SimpleListFilterParent
   *
   * @param string $filter_name
   *   The name of the class
   * @param stdClass $caching_engine
   *   A caching engine object to pass to the new class on construct.
   * @return SimpleListFilterParent
   *   The filter object to use.
   */
  public function getFilter($filter_name, $caching_engine) {
    simplelist_require_all_filter_classes();
    /*require_once(drupal_get_path('module', 'simplelist') .'/SimpleListNodeFilter.php');
    require_once(drupal_get_path('module', 'simplelist') .'/SimpleListFilterNodeByUser.php');*/
    
    if (class_exists($filter_name)) {
      $filter = new $filter_name($caching_engine);
      return $this->checkDynamicClass($filter, 'SimpleListFilterParent');
    } 
    else {
      drupal_set_message("Class $filter_name for simplelist filtering is not defined!", 'error');
      watchdog('simplelist', 'Class @filter_name for simplelist filtering is not defined.', array('@filter_name' => $filter_name), WATCHDOG_ERROR);
    }
    return NULL;
  }
  
  /**
   * Dynamically loads the filter, making sure that it can be loaded and that the loaded class is descended from SimpleListDisplayParent
   *
   * @param string $display_name
   *   Name of the class to load
   * @return SimpleListDisplayParent
   *   The display object to use.
   */
  public function getDisplay($display_name) {
    require_once(drupal_get_path('module', 'simplelist') .'/SimpleListDisplay.php');
    require_once(drupal_get_path('module', 'simplelist') .'/SimpleListDisplayCascade.php');
    
    if (class_exists($display_name)) {
      $display = new $display_name();
      return $this->checkDynamicClass($display, 'SimpleListDisplayParent');
    } 
    else {
      drupal_set_message("Class $display_name for simplelist display is not defined!", 'error');
      watchdog('simplelist', 'Class @filter_name for simplelist display is not defined.', array('@filter_name' => $display_name), WATCHDOG_ERROR);
    }
    return NULL;
  }
  
  /**
   * Checks to make sure that a given object is the class we want.  If not, errors are written to dsm and watchdog.
   *
   * @param object $object
   *   Object to inspect
   * @param string $class_name
   *   Desired class name
   * @return object
   *   Either the object, or NULL, based on the result.
   */
  private function checkDynamicClass($object, $class_name) {
    if (is_a($object, $class_name)) {
      return $object;
    } 
    else {
      $actual_class_name = get_class($object);
      drupal_set_message("Class $actual_class_name for simplelist is not a $class_name!", 'error');
      watchdog('simplelist', 'Class @class_name for simplelist is not a @parent_name.', array('@class_name' => $actual_class_name, '@parent_name' => $class_name), WATCHDOG_ERROR);
    }
    return NULL;
  }
  
  /**
   * Invokes a hook among different modules, passing in this list.
   *
   * @param string $hook
   *   Name of the hook to invoke.
   */
  private function callSimpleListHook($hook) {
    foreach(module_implements($hook) as $module) {
      module_invoke($module, $hook, $this->simplelist);
    }
  }
  
  /**
   * Invoke a hook among different modules, passing in this list, and accumulating the returned strings.
   *
   * @param string $hook
   *   Name of the hook to invoke
   * @return string
   *   The accumulated string.
   */
  private function callSimpleListHookReturnString($hook) {
    $return_string = '';
    foreach(module_implements($hook) as $module) {
      $return_string .= module_invoke($module, $hook, $this->simplelist);
    }
    return $return_string;
  }
}
?>