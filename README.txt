Simplelist
----------

   Simplelist is a simple method of getting lists of content on your site.  It doesn't try to do everything, instead 
it relies on node_load to pull in node data and node_view or a theme function to display it.  As such, it doesn't 
require any special coding to use a custom module with Simplelist - it just works.  To handle the problem of how node_load
can be slower than a query, with the various nodeapi calls, we cache each node temporarily, and use that value until it's
invalidated.  (Creating a new node or editing a node will invalidate all of these cached nodes, so the cache time generally
won't be very long.)

   With future expansion, content other than nodes will be presented in simplelist - there's nothing inherently limiting
a simplelist to dealing with nodes.  See DEVELOPER.txt for information for expanding simplelist to do more than the basics. 

  Important paths:
  admin/build/simplelist -> This will take you to a page listing all of the simplelists you've defined.  From here you
can add more lists, edit or clone existing lists, or delete any of the lists.  (Cloning just makes a copy of an existing list,
and prefills an add page with the data.  Note that you've got to change the name, at least.)

  admin/build/simplelist/add -> This will let you add a simplelist.

   Using Simplelist is pretty easy.  When you create one you're given a form with a number of settings - the first group
will be the same for all lists, and consists of the following items:

Name:
  This is the name of your simplelist, which needs to be unique and contain only alphanumeric characters plus the underscore.
If you're creating a block with this simplelist, and you don't get the block a title, then this name will display on the blocks page.

Access:
  Indicates which roles will see the block or page.  If you leave them all unchecked, everyone will be able to see them.
  
Description:
  A note to yourself what this is for.  It also displays on the list page (admin/build/simplelist).
  
Block Caching:
  If you've got a block based on this simplelist, then this control lets you decide how the block is cached.
  
Show Published Nodes?:
  This control lets you decide if the simplelist will show only published nodes, only unpublished nodes, or all nodes
published or not.  99% of the time, you want to show the published nodes only, so that's the default.

  The Filter Options section lets you decide which nodes will display on your list and the order they display in.  The 
default filter is the Simple Node Filter, which lets you choose any node types and taxonomy terms - neither is required,
so if you want all nodes to show up in the results just select nothing.  The 'Filter Node By User' comes with the module
as well, and lets you choose nodes by node type, user role, and/or Created by.  Choosing one or more user roles mean
that you only want to see nodes created by users who have those roles.  (You can choose both User Role and Created by,
but if you choose only roles that the Created by user doesn't have, no nodes will be returned.  So, it's a little silly.)

  The Filter options also contains the Sort Order and Sort Direction - the defaults on these are 'Date Created' and 
'Descending' because that's what I tend to use the most.  Other options include Date Updated, Title, Node ID, Author Name,
and Node Type.  If comments are enabled 'Comment Count' is an option, and if you've got the voting api installed it'll
allow you to choose 'highest rated'.  Not all filters will provide the same sorting options - the simplelist_nodequeue filter,
for instance, only allows you to sort by nodequeue order.

Display Options:

  The display options give you the ability to devine a block, a page, or both for the simplelist.  You've got to choose at least
one of them.  

Block Display Options:

  Use Block?
    If checked, simplelist will generate a block.
    
  Display:
    Here you get to decide how to display your simplelist.  Currently your options are 'Display as List', which presents your
  nodes in an unordered list, or 'Display as a series of items', which is where each node is displayed after each other.  Both
  of these have the same parameters, but future displays may have different form fields for them.
  
  Block Title:
    The title of your block.  This is displayed both above your block, and on the block page.
    
  Block Item Count:
    How many items to display on your block.  
    
  Display 'More' link checkbox:
    If you select this, you are required to have a page view for the same simplelist.  But if selected, the block will end 
  with a link that brings the user to your simplelist's page.
  
  Node Format:
    Your choice of how to show the nodes - either 'As Teaser' - standard node teaser; 'As Full Node' - standard full node view;
  'As Themed' - as presented by a theme function you provide, or 'As Titles, linking to node' - which is just the title of the
  node, set up as a link to the node's page.
  
    The theme name is 'simplelist__<name of list>' - so, if your simplelist's name is 'list_of_blogs', for instance, 
  then the theme function is 'simplelist__list_of_blogs' and would be defined as 
  function <themename>_simplelist__list_of_blogs($node, $simplelist) {...} .
  
    If you want a catch-all theme function for your simplelists, if simplelist__<name> fails, it'll try for just 'simplelist', or
  function <themename>_simplelist($node, $simplelist) {...}.  The default simplelist theme is currently just printing out the
  title of the node, without any link, so if that's what you're getting, you've either not defined your theme properly or chosen 
  the wrong node format.
  
Page Display Options:

  Use Page? 
    If checked, simplelist will provide a page for the nodes to display on.
    
  Display:
    Here you get to decide how to display your simplelist.  Currently your options are 'Display as List', which presents your
  nodes in an unordered list, or 'Display as a series of items', which is where each node is displayed after each other.  Both
  of these have the same parameters, but future displays may have different form fields for them.
  
  Page Title:
    The title to display on the page.
    
  Page Item Count:
    The number of items to display on the page, or on each page if you select 'Display pager'.
  
  Display pager at bottom of page?
    If checked, then a pager will be provided at the bottom of the page.
    
  Node Format:
      Your choice of how to show the nodes - either 'As Teaser' - standard node teaser; 'As Full Node' - standard full node view;
  'As Themed' - as presented by a theme function you provide, or 'As Titles, linking to node' - which is just the title of the
  node, set up as a link to the node's page.
  
    The theme name is 'simplelist__<name of list>' - so, if your simplelist's name is 'list_of_blogs', for instance, 
  then the theme function is 'simplelist__list_of_blogs' and would be defined as 
  function <themename>_simplelist__list_of_blogs($node, $simplelist) {...} .
  
    If you want a catch-all theme function for your simplelists, if simplelist__<name> fails, it'll try for just 'simplelist', or
  function <themename>_simplelist($node, $simplelist) {...}.  The default simplelist theme is currently just printing out the
  title of the node, without any link, so if that's what you're getting, you've either not defined your theme properly or chosen 
  the wrong node format.
  
    Yes, I did just copy/paste that from the Block Node Format.
    
  Page Path:
    This is the path to display the page at.
    
    There's two arguments you can include in the path to dynamically choose your page.  The first is %tid to include the term
  id in the path.  The second is $node_type for the node type.  When the page is reached, the filter will be told to ignore the
  already chosen term or node type, and will instead use the data from the path.  Don't use these arguments in the first section
  of the path.
  
    For example, if I have a vocabulary with the terms (4 => Red, 5 => Blue, 6 => Green), then if my path is 'testpath/%tid', then  
  going to 'testpath/5' will show me all of the Blue nodes, even if I specificly chose Green in the filter.  
    
    Another example: If you set up a path of 'testmore/%node_type/%tid', then going to 'testmore/blog/4' will show you all of the
  Red blog entries.
  
    Note well: Currently, the arguments only work if you choose a filter that uses those terms - for instance, if you choose a 
  filter that doesn't handle taxonomy terms, putting %tid in your page path will break.  I'm planning a change in the future which
  passes the responsibilities of the page path more firmly to the filters, but it's not done yet.
  
 