<?php

abstract class SimpleListDisplayParent {
  public function __construct() {
  }
  
  /**
   * The submit function for handling the display classes form.  
   *
   * @param unknown_type $form_id
   * @param unknown_type $form_state
   * @param unknown_type $format
   * @return unknown
   */
  public static function get_display_form_submit($form_id, &$form_state, $format='block') {
    $find_query = "SELECT sldid FROM {simplelist_display} WHERE slid = %d AND display_context = '%s'";
    $slid = $form_state['values']['slid'];
    $result = db_query($find_query, $slid, $format);
    if ($row = db_fetch_object($result)) {
      $sldid = $row->sldid;
    }
    else {
      $sldid = 0;
    }
    
    $insert_display_query = "INSERT INTO {simplelist_display} (slid, display_name, display_context, display_title, display_count, display_pager, display_more, display_format, display_path) ".
      "VALUES (%d, '%s', '%s', '%s', %d, %d, %d, %d, '%s')";
    $update_display_query = "UPDATE {simplelist_display} SET display_name = '%s', display_title = '%s', display_count = %d, display_pager = %d, ".
      "display_more = %d, display_format = %d, display_path = '%s' WHERE slid = %d AND display_context = '%s'";
    $delete_display_query = "DELETE FROM {simplelist_display} WHERE slid = %d AND display_context = '%s'";
    if ($format == 'block') {
      if (!$sldid && $form_state['values']['block_select']) {
        // insert!
        db_query($insert_display_query, $slid, $form_state['values']['block_display_name'], "block", $form_state['values']['block_display_title'],
          $form_state['values']['block_display_count'], 0, $form_state['values']['block_display_more'], $form_state['values']['block_display_format'],
          $form_state['values']['block_display_path']);
        return TRUE;
      }
      else if ($sldid) { // If there's already a row in simplelist_display...
        if ($form_state['values']['block_select']) {
          // UPDATE!
          db_query($update_display_query, $form_state['values']['block_display_name'], $form_state['values']['block_display_title'],
            $form_state['values']['block_display_count'], 0, $form_state['values']['block_display_more'], $form_state['values']['block_display_format'],
            $form_state['values']['block_display_path'], $slid, 'block');
          return TRUE;
        }
        else {
          // DELETE!
          db_query($delete_display_query, $slid, 'block');
          return TRUE;
        }
      }
    }
    else if ($format == 'page') {
      if (!$sldid && $form_state['values']['page_select']) {
        // insert!
        db_query($insert_display_query, $slid, $form_state['values']['page_display_name'], "page", $form_state['values']['page_display_title'],
          $form_state['values']['page_display_count'], $form_state['values']['page_display_pager'], 0, $form_state['values']['page_display_format'],
          $form_state['values']['page_display_path']);
        return TRUE;
      }
      else if ($sldid) { // If there's already a row in simplelist_display...
        if ($form_state['values']['page_select']) {
          // UPDATE!
        db_query($update_display_query, $form_state['values']['page_display_name'], $form_state['values']['page_display_title'],
          $form_state['values']['page_display_count'], $form_state['values']['page_display_pager'], 0, $form_state['values']['page_display_format'],
          $form_state['values']['page_display_path'], $slid, 'page');
          return TRUE;
        }
        else {
          // DELETE!
          db_query($delete_display_query, $slid, 'page');
          return TRUE;
        }
      }
    }
    return FALSE;
  }
  
  /**
   * Validation function for the class' form elements.
   *
   * @param unknown_type $form
   * @param unknown_type $form_state
   * @param unknown_type $format
   */
  public static function get_display_form_validate(&$form, &$form_state, $format='block') {
    if ($format == 'block') {
      $block_count = $form_state['values']['block_display_count'];
      if (!_simplelist_validate_numeric($block_count)) {
        form_set_error('block_display_count', "Block count has to be a number if you're generating a block.");
      }
    }
    else if ($format == 'page') {
      if ($form_state['values']['page_select']) {
        $page_count = $form_state['values']['page_display_count'];
        if (!_simplelist_validate_numeric($page_count)) {
          form_set_error('page_display_count', "Page count has to be a number if you're generating a page.");
        }
        
        if ($form_state['values']['page_display_path'] == '') {
          form_set_error('page_display_path', "If you're creating a page-based simplelist, you need to provide a path.");
        }
      }
    } 
  }
  
  abstract public function render($simple_list, $node_array);
  abstract public static function get_display_form($simplelist, $format='block');
  //abstract public static function get_display_form_validate(&$form, &$form_state, $format='block');
  abstract public static function clear_existing_settings($slid, $form_id='', &$form_state=NULL, $format='block');
}
?>