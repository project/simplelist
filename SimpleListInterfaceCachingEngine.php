<?php

/*
 * Yes, this is a tiny little interface.  It's needed for testing.
 */
interface SimpleListInterfaceCachingEngine{
  public function fetch_node($nid);
}
?>