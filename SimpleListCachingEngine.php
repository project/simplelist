<?php

require_once(drupal_get_path('module', 'simplelist') .'/SimpleListInterfaceCachingEngine.php');
/**
 * The caching engine uses the standard drupal cache functions to store loaded nodes for a short time.  This keeps down the
 * possibly complicated load-times that happen when a node is loaded by passing it around through nodeapi.
 *
 * We save the cached data in cache_block, because it will be invalidated when a node is updated or inserted.
 */
class SimpleListCachingEngine implements SimpleListInterfaceCachingEngine {
  public function fetch_node($nid) {
    $cache_data = cache_get($this->get_node_cache_id($nid), 'cache_block');
    //drupal_set_message('cache:'. dprint_r($cache_data, true));
    if ($cache_data) {
      return $cache_data->data;
    }
    else {
      $node = node_load($nid);
      cache_set($this->get_node_cache_id($nid), $node, 'cache_block', CACHE_TEMPORARY); 
      return $node;
    }
  }
  
  public function get_node_cache_id($nid) {
    global $user;
    return 'node_'. implode(',', array_keys($user->roles)) .'_'. $nid;
  }
}
?>