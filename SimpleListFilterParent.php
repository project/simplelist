<?php

abstract class SimpleListFilterParent {
  protected $cache_engine;
  
  public function __construct($cache_engine) {
    $this->cache_engine = $cache_engine;  
  }
  
  protected function get_sort_order_from_sort_data($sort_data) {
    return ($sort_data == 'DESC' || $sort_data == 'ASC' ? $sort_data : '');
  }
  
  abstract public function get_node_list($simple_list, $count, $offset, $paged);
  abstract public static function get_filter_form($simplelist);
  abstract public static function get_filter_form_validate(&$form, &$form_state);
  abstract public static function get_filter_form_submit($form_id, &$form_state);
  abstract public static function clear_existing_settings($slid, $form_id='', &$form_state=NULL);
}
?>