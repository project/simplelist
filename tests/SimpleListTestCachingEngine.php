<?php

require_once(drupal_get_path('module', 'simplelist') .'/SimpleListInterfaceCachingEngine.php');
/**
 * Simple mock framework for caching engine - passes pre-made objects to the caller through fetch_node, allows you to check that nodes were asked for.
 *
 */
class SimpleListTestCachingEngine implements SimpleListInterfaceCachingEngine {
  private $test_data;
  
  public function __construct($test_data) {
    $this->test_data = $test_data;
    foreach($this->test_data as $key=>$data) {
      $this->test_data[$key]->mock_check_use = FALSE;
    }
  }
  
  public function fetch_node($nid) {
    if (isset($this->test_data[$nid])) {
      $this->test_data[$nid]->mock_check_use = TRUE;
      return $this->test_data[$nid];
    }
    else {
      $this->test_data[$nid] = new stdClass();
      $this->test_data[$nid]->mock_check_use = TRUE;
      return NULL;
    }
  }
  
  public function test_fetch_node($nid) {
    if (isset($this->test_data[$nid])) {
      return $this->test_data[$nid]->mock_check_use ? TRUE : FALSE;
    } 
    else {
      return FALSE; // If it's not in the array, we didn't ask for it.
    }
  }
  
  public function test_fetch_node_array($value = 'all') {
    $result = array();
    foreach($this->test_data as $key=>$data) {
      if ($value === 'all' || $value === $data->mock_check_use) {
        $result[$key] = $data->mock_check_use;
      }
    }
    return $result;
  }
}