<?php

/*
 * This test class contains tests that try to create a simplelist in the wrong way, thus causing it to fail and error out, verifying that the error did appear.
 */
class simplelist_test_validation extends DrupalTestCase {
  /**
   * Implementation of get_info() for information
   */
  function get_info() {
    return array('name' => t('Simplelist Test Validation'), 'desc' => 'Attempts to enter a simpletest wrong to make sure the validation fires.', 'group' => 'SimpleList');
  }

  function tearDown() {
    parent::tearDown();
  }
  
  function get_taxonomy_terms($start = 0) {
    $query = "SELECT t.tid FROM {term_data} t INNER JOIN {vocabulary} v ON (t.vid = v.vid) ORDER BY t.tid";
    $result = db_query_range($query, $start, 3);
    $return = array();
    while ($term = db_fetch_object($result)) {
      $return[] = $term->tid;
    }
    return $return;
  }

  function test_missing_name_triggers_error() {
    $simplelist_name = '';
    $simplelist_desc = $this->randomName(26, 'simplelist_desc_');
    $web_user = $this->drupalCreateUserRolePerm(array (
      0 => 'access comments',
      1 => 'access content',
      2 => 'administer simplelist',
    ));
    $this->drupalLoginUser($web_user);
    $this->drupalGet(url('admin/build/simplelist', array("absolute" => TRUE)));
    $this->assertWantedRaw('Existing Simplelists', 'Existing Simplelists');
    $this->assertWantedRaw('Administer Simplelists', "Page title");

    $this->clickLink('Add', '0');
    $this->assertWantedRaw('Add a Simplelist', "Page title");
    
    $test_terms = $this->get_taxonomy_terms(0);
    $edit = array (
      'name' => $simplelist_name,
      'access[1]' => '1',
      'access[2]' => '2',
      'description' => $simplelist_desc,
      'cache' => BLOCK_NO_CACHE,
      'published' => SIMPLELIST_PUBLISHED_NODES,
      'filter_name' => 'SimpleListNodeFilter',
      'node_types[blog]' => 'blog',
      'node_types[image]' => 'image',
      'node_types[page]' => 'page',
      'taxonomy_tids[]' => $test_terms,
      'sort_name' => 'title',
      'sort_data' => 'ASC',
      'block_select' => '1',
      'block_display_name' => 'SimpleListDisplay',
      'block_display_title' => 'Simplelist Block',
      'block_display_count' => '10',
      'block_display_more' => '1',
      'block_display_format' => SIMPLELIST_FORMAT_TEASER,
      'page_select' => '1',
      'page_display_name' => 'SimpleListDisplay',
      'page_display_title' => 'Simplelist Page',
      'page_display_count' => '10',
      'page_display_pager' => '1',
      'page_display_format' => SIMPLELIST_FORMAT_TEASER,
      'page_display_path' => $simplelist_name,
    );
    $this->drupalPost('admin/build/simplelist/add', $edit, 'Save');
    
    $this->assertWantedRaw('Name field is required.', '[simplelist] Verify that name is required. %s');
  }
  
  function test_invalid_name_triggers_error() {
    $simplelist_name = 'question?';
    $simplelist_desc = $this->randomName(26, 'simplelist_desc_');
    $web_user = $this->drupalCreateUserRolePerm(array (
      0 => 'access comments',
      1 => 'access content',
      2 => 'administer simplelist',
    ));
    $this->drupalLoginUser($web_user);
    $this->drupalGet(url('admin/build/simplelist', array("absolute" => TRUE)));
    $this->assertWantedRaw('Existing Simplelists', 'Existing Simplelists');
    $this->assertWantedRaw('Administer Simplelists', "Page title");

    $this->clickLink('Add', '0');
    $this->assertWantedRaw('Add a Simplelist', "Page title");
    $test_terms = $this->get_taxonomy_terms(0);
    $edit = array (
      'name' => $simplelist_name,
      'access[1]' => '1',
      'access[2]' => '2',
      'description' => $simplelist_desc,
      'cache' => BLOCK_NO_CACHE,
      'published' => SIMPLELIST_PUBLISHED_NODES,
      'filter_name' => 'SimpleListNodeFilter',
      'node_types[blog]' => 'blog',
      'node_types[image]' => 'image',
      'node_types[page]' => 'page',
      'taxonomy_tids[]' => $test_terms,
      'sort_name' => 'title',
      'sort_data' => 'ASC',
      'block_select' => '1',
      'block_display_name' => 'SimpleListDisplay',
      'block_display_title' => 'Simplelist Block',
      'block_display_count' => '10',
      'block_display_more' => '1',
      'block_display_format' => SIMPLELIST_FORMAT_TEASER,
      'page_select' => '1',
      'page_display_name' => 'SimpleListDisplay',
      'page_display_title' => 'Simplelist Page',
      'page_display_count' => '10',
      'page_display_pager' => '1',
      'page_display_format' => SIMPLELIST_FORMAT_TEASER,
      'page_display_path' => $simplelist_name,
    );
    $this->drupalPost('admin/build/simplelist/add', $edit, 'Save');
    
    $this->assertWantedRaw('Simplelist Names can only contain the letters a-z, numbers, and the underscore character.', '[simplelist] Verify that name is invalid. %s');
  }
  
  function test_display_required() {
    $simplelist_name = $this->randomName(6, 'simplelist_test_');
    $simplelist_desc = $this->randomName(26, 'simplelist_desc_');
    $web_user = $this->drupalCreateUserRolePerm(array (
      0 => 'access comments',
      1 => 'access content',
      2 => 'administer simplelist',
    ));
    $this->drupalLoginUser($web_user);
    $this->drupalGet(url('admin/build/simplelist', array("absolute" => TRUE)));
    $this->assertWantedRaw('Existing Simplelists', 'Existing Simplelists');
    $this->assertWantedRaw('Administer Simplelists', "Page title");

    $this->clickLink('Add', '0');
    $this->assertWantedRaw('Add a Simplelist', "Page title");
    $test_terms = $this->get_taxonomy_terms(0);
    $edit = array (
      'name' => $simplelist_name,
      'access[1]' => '1',
      'access[2]' => '2',
      'description' => $simplelist_desc,
      'cache' => BLOCK_NO_CACHE,
      'published' => SIMPLELIST_PUBLISHED_NODES,
      'filter_name' => 'SimpleListNodeFilter',
      'node_types[blog]' => 'blog',
      'node_types[image]' => 'image',
      'node_types[page]' => 'page',
      'taxonomy_tids[]' => $test_terms,
      'sort_name' => 'title',
      'sort_data' => 'ASC',
      //'block_select' => '1',
      'block_display_name' => 'SimpleListDisplay',
      'block_display_title' => 'Simplelist Block',
      'block_display_count' => '10',
      'block_display_more' => '1',
      'block_display_format' => SIMPLELIST_FORMAT_TEASER,
      //'page_select' => '1',
      'page_display_name' => 'SimpleListDisplay',
      'page_display_title' => 'Simplelist Page',
      'page_display_count' => '10',
      'page_display_pager' => '1',
      'page_display_format' => SIMPLELIST_FORMAT_TEASER,
      'page_display_path' => $simplelist_name,
    );
    $this->drupalPost('admin/build/simplelist/add', $edit, 'Save');
    
    $this->assertWantedRaw('A simplelist requires either the <em>Use Block?</em> or <em>Use Page?</em> to be displayed.', '[simplelist] Verify that name is required. %s');
  }
  
  function test_missing_block_count_required() {
    $simplelist_name = $this->randomName(6, 'simplelist_test_');
    $simplelist_desc = $this->randomName(26, 'simplelist_desc_');
    $web_user = $this->drupalCreateUserRolePerm(array (
      0 => 'access comments',
      1 => 'access content',
      2 => 'administer simplelist',
    ));
    $this->drupalLoginUser($web_user);
    $this->drupalGet(url('admin/build/simplelist', array("absolute" => TRUE)));
    $this->assertWantedRaw('Existing Simplelists', 'Existing Simplelists');
    $this->assertWantedRaw('Administer Simplelists', "Page title");

    $this->clickLink('Add', '0');
    $this->assertWantedRaw('Add a Simplelist', "Page title");
    $test_terms = $this->get_taxonomy_terms(0);
    $edit = array (
      'name' => $simplelist_name,
      'access[1]' => '1',
      'access[2]' => '2',
      'description' => $simplelist_desc,
      'cache' => BLOCK_NO_CACHE,
      'published' => SIMPLELIST_PUBLISHED_NODES,
      'filter_name' => 'SimpleListNodeFilter',
      'node_types[blog]' => 'blog',
      'node_types[image]' => 'image',
      'node_types[page]' => 'page',
      'taxonomy_tids[]' => $test_terms,
      'sort_name' => 'title',
      'sort_data' => 'ASC',
      'block_select' => '1',
      'block_display_name' => 'SimpleListDisplay',
      'block_display_title' => 'Simplelist Block',
      'block_display_count' => '',
      //'block_display_more' => '0',
      'block_display_format' => SIMPLELIST_FORMAT_TEASER,
      /*'page_select' => '1',
      'page_display_name' => 'SimpleListDisplay',
      'page_display_title' => 'Simplelist Page',
      'page_display_count' => '10',
      'page_display_pager' => '1',
      'page_display_format' => SIMPLELIST_FORMAT_TEASER,
      'page_display_path' => $simplelist_name,*/
    );
    $this->drupalPost('admin/build/simplelist/add', $edit, 'Save');
    
    $this->assertWantedRaw("Block count has to be a number if you're generating a block.", 
      '[simplelist] Verify that block count is required. %s');
  }
  
  function test_invalid_block_count() {
    $simplelist_name = $this->randomName(6, 'simplelist_test_');
    $simplelist_desc = $this->randomName(26, 'simplelist_desc_');
    $web_user = $this->drupalCreateUserRolePerm(array (
      0 => 'access comments',
      1 => 'access content',
      2 => 'administer simplelist',
    ));
    $this->drupalLoginUser($web_user);
    $this->drupalGet(url('admin/build/simplelist', array("absolute" => TRUE)));
    $this->assertWantedRaw('Existing Simplelists', 'Existing Simplelists');
    $this->assertWantedRaw('Administer Simplelists', "Page title");

    $this->clickLink('Add', '0');
    $this->assertWantedRaw('Add a Simplelist', "Page title");
    $test_terms = $this->get_taxonomy_terms(0);
    $edit = array (
      'name' => $simplelist_name,
      'access[1]' => '1',
      'access[2]' => '2',
      'description' => $simplelist_desc,
      'cache' => BLOCK_NO_CACHE,
      'published' => SIMPLELIST_PUBLISHED_NODES,
      'filter_name' => 'SimpleListNodeFilter',
      'node_types[blog]' => 'blog',
      'node_types[image]' => 'image',
      'node_types[page]' => 'page',
      'taxonomy_tids[]' => $test_terms,
      'sort_name' => 'title',
      'sort_data' => 'ASC',
      'block_select' => '1',
      'block_display_name' => 'SimpleListDisplay',
      'block_display_title' => 'Simplelist Block',
      'block_display_count' => '12f',
      //'block_display_more' => '0',
      'block_display_format' => SIMPLELIST_FORMAT_TEASER,
      /*'page_select' => '1',
      'page_display_name' => 'SimpleListDisplay',
      'page_display_title' => 'Simplelist Page',
      'page_display_count' => '10',
      'page_display_pager' => '1',
      'page_display_format' => SIMPLELIST_FORMAT_TEASER,
      'page_display_path' => $simplelist_name,*/
    );
    $this->drupalPost('admin/build/simplelist/add', $edit, 'Save');
    
    $this->assertWantedRaw("Block count has to be a number if you're generating a block.", 
      '[simplelist] Verify that block count is numeric. %s');
  }

  function test_missing_page_count_required() {
    $simplelist_name = $this->randomName(6, 'simplelist_test_');
    $simplelist_desc = $this->randomName(26, 'simplelist_desc_');
    $web_user = $this->drupalCreateUserRolePerm(array (
      0 => 'access comments',
      1 => 'access content',
      2 => 'administer simplelist',
    ));
    $this->drupalLoginUser($web_user);
    $this->drupalGet(url('admin/build/simplelist', array("absolute" => TRUE)));
    $this->assertWantedRaw('Existing Simplelists', 'Existing Simplelists');
    $this->assertWantedRaw('Administer Simplelists', "Page title");

    $this->clickLink('Add', '0');
    $this->assertWantedRaw('Add a Simplelist', "Page title");
    $test_terms = $this->get_taxonomy_terms(0);
    $edit = array (
      'name' => $simplelist_name,
      'access[1]' => '1',
      'access[2]' => '2',
      'description' => $simplelist_desc,
      'cache' => BLOCK_NO_CACHE,
      'published' => SIMPLELIST_PUBLISHED_NODES,
      'filter_name' => 'SimpleListNodeFilter',
      'node_types[blog]' => 'blog',
      'node_types[image]' => 'image',
      'node_types[page]' => 'page',
      'taxonomy_tids[]' => $test_terms,
      'sort_name' => 'title',
      'sort_data' => 'ASC',
      /*'block_select' => '1',
      'block_display_name' => 'SimpleListDisplay',
      'block_display_title' => 'Simplelist Block',
      'block_display_count' => '',
      //'block_display_more' => '0',
      'block_display_format' => SIMPLELIST_FORMAT_TEASER,*/
      'page_select' => '1',
      'page_display_name' => 'SimpleListDisplay',
      'page_display_title' => 'Simplelist Page',
      'page_display_count' => '',
      'page_display_pager' => '1',
      'page_display_format' => SIMPLELIST_FORMAT_TEASER,
      'page_display_path' => $simplelist_name,
    );
    $this->drupalPost('admin/build/simplelist/add', $edit, 'Save');
    
    $this->assertWantedRaw("Page count has to be a number if you're generating a page.", 
      '[simplelist] Verify that page count is required. %s');
  }
  
  function test_invalid_page_count() {
    $simplelist_name = $this->randomName(6, 'simplelist_test_');
    $simplelist_desc = $this->randomName(26, 'simplelist_desc_');
    $web_user = $this->drupalCreateUserRolePerm(array (
      0 => 'access comments',
      1 => 'access content',
      2 => 'administer simplelist',
    ));
    $this->drupalLoginUser($web_user);
    $this->drupalGet(url('admin/build/simplelist', array("absolute" => TRUE)));
    $this->assertWantedRaw('Existing Simplelists', 'Existing Simplelists');
    $this->assertWantedRaw('Administer Simplelists', "Page title");

    $this->clickLink('Add', '0');
    $this->assertWantedRaw('Add a Simplelist', "Page title");
    $test_terms = $this->get_taxonomy_terms(0);
    $edit = array (
      'name' => $simplelist_name,
      'access[1]' => '1',
      'access[2]' => '2',
      'description' => $simplelist_desc,
      'cache' => BLOCK_NO_CACHE,
      'published' => SIMPLELIST_PUBLISHED_NODES,
      'filter_name' => 'SimpleListNodeFilter',
      'node_types[blog]' => 'blog',
      'node_types[image]' => 'image',
      'node_types[page]' => 'page',
      'taxonomy_tids[]' => $test_terms,
      'sort_name' => 'title',
      'sort_data' => 'ASC',
      /*'block_select' => '1',
      'block_display_name' => 'SimpleListDisplay',
      'block_display_title' => 'Simplelist Block',
      'block_display_count' => '12f',
      //'block_display_more' => '0',
      'block_display_format' => SIMPLELIST_FORMAT_TEASER,*/
      'page_select' => '1',
      'page_display_name' => 'SimpleListDisplay',
      'page_display_title' => 'Simplelist Page',
      'page_display_count' => '12f',
      'page_display_pager' => '1',
      'page_display_format' => SIMPLELIST_FORMAT_TEASER,
      'page_display_path' => $simplelist_name,
    );
    $this->drupalPost('admin/build/simplelist/add', $edit, 'Save');
    
    $this->assertWantedRaw("Page count has to be a number if you're generating a page.", 
      '[simplelist] Verify that page count is numeric. %s');
  }
  
  function test_invalid_page_path_count() {
    $simplelist_name = $this->randomName(6, 'simplelist_test_');
    $simplelist_desc = $this->randomName(26, 'simplelist_desc_');
    $web_user = $this->drupalCreateUserRolePerm(array (
      0 => 'access comments',
      1 => 'access content',
      2 => 'administer simplelist',
    ));
    $this->drupalLoginUser($web_user);
    $this->drupalGet(url('admin/build/simplelist', array("absolute" => TRUE)));
    $this->assertWantedRaw('Existing Simplelists', 'Existing Simplelists');
    $this->assertWantedRaw('Administer Simplelists', "Page title");

    $this->clickLink('Add', '0');
    $this->assertWantedRaw('Add a Simplelist', "Page title");
    $test_terms = $this->get_taxonomy_terms(0);
    $edit = array (
      'name' => $simplelist_name,
      'access[1]' => '1',
      'access[2]' => '2',
      'description' => $simplelist_desc,
      'cache' => BLOCK_NO_CACHE,
      'published' => SIMPLELIST_PUBLISHED_NODES,
      'filter_name' => 'SimpleListNodeFilter',
      'node_types[blog]' => 'blog',
      'node_types[image]' => 'image',
      'node_types[page]' => 'page',
      'taxonomy_tids[]' => $test_terms,
      'sort_name' => 'title',
      'sort_data' => 'ASC',
      'block_select' => '1',
      'block_display_name' => 'SimpleListDisplay',
      'block_display_title' => 'Simplelist Block',
      'block_display_count' => '12f',
      //'block_display_more' => '0',
      'block_display_format' => SIMPLELIST_FORMAT_TEASER,
      'page_select' => '1',
      'page_display_name' => 'SimpleListDisplay',
      'page_display_title' => 'Simplelist Page',
      'page_display_count' => '10',
      'page_display_pager' => '1',
      'page_display_format' => SIMPLELIST_FORMAT_TEASER,
      'page_display_path' => '',
    );
    $this->drupalPost('admin/build/simplelist/add', $edit, 'Save');
    
    $this->assertWantedRaw("If you're creating a page-based simplelist, you need to provide a path.", 
      '[simplelist] Verify that block count is numeric. %s');
  }
  
  /**
   * This test is here because at one point I coded the module so it would only allow simplelists with both blocks and page, where I wanted
   *   blocks or page.
   *
   */
  function test_allow_only_block_count() {
    $simplelist_name = $this->randomName(6, 'simplelist_test_');
    $simplelist_desc = $this->randomName(26, 'simplelist_desc_');
    $web_user = $this->drupalCreateUserRolePerm(array (
      0 => 'access comments',
      1 => 'access content',
      2 => 'administer simplelist',
    ));
    $this->drupalLoginUser($web_user);
    $this->drupalGet(url('admin/build/simplelist', array("absolute" => TRUE)));
    $this->assertWantedRaw('Existing Simplelists', 'Existing Simplelists');
    $this->assertWantedRaw('Administer Simplelists', "Page title");

    $this->clickLink('Add', '0');
    $this->assertWantedRaw('Add a Simplelist', "Page title");
    $test_terms = $this->get_taxonomy_terms(0);
    $edit = array (
      'name' => $simplelist_name,
      'access[1]' => '1',
      'access[2]' => '2',
      'description' => $simplelist_desc,
      'cache' => BLOCK_NO_CACHE,
      'published' => SIMPLELIST_PUBLISHED_NODES,
      'filter_name' => 'SimpleListNodeFilter',
      'node_types[blog]' => 'blog',
      'node_types[image]' => 'image',
      'node_types[page]' => 'page',
      'taxonomy_tids[]' => $test_terms,
      'sort_name' => 'title',
      'sort_data' => 'ASC',
      'block_select' => '1',
      'block_display_name' => 'SimpleListDisplay',
      'block_display_title' => 'Simplelist Block',
      'block_display_count' => '5',
      //'block_display_more' => '0',
      'block_display_format' => SIMPLELIST_FORMAT_TEASER,
      /*'page_select' => '1',
      'page_display_name' => 'SimpleListDisplay',
      'page_display_title' => 'Simplelist Page',
      'page_display_count' => '10',
      'page_display_pager' => '1',
      'page_display_format' => SIMPLELIST_FORMAT_TEASER,
      'page_display_path' => $simplelist_name,*/
    );
    $this->drupalPost('admin/build/simplelist/add', $edit, 'Save');
    
    $this->assertNoUnwantedRaw('A simplelist requires either the <em>Use Block?</em> or <em>Use Page?</em> to be displayed.', 
      '[simplelist] Verify that we can create a simplelist with just a block. %s');
    $this->assertWantedRaw('Simplelist '. $simplelist_name .' has been created', '[simplelist] Simplelist was created. %s');
    
    $simplelist = simplelist_load($simplelist_name);
    $this->assertNotNull($simplelist, '[simplelist] Should retrieve valid simplelist from database. %s');
    
    if ($simplelist) {
      $slid = $simplelist->slid;
      
      $edit = array ();
      $this->drupalPost('admin/build/simplelist/delete/'. $simplelist->slid, $edit, 'Delete');
      
      // Need test that item no longer shows here.
      $this->assertWantedRaw('Administer Simplelists', "Page title");
      $this->assertNoUnwantedRaw($simplelist_name, '[simplelist] No longer find simplelist in list.');
      
      $this->assertNull(simplelist_load($simplelist_name), '[simplelist] Cannot get simplelist from database. %s');
    }
  }
  
  function test_name_unique() {
    $simplelist_name = db_result(db_query_range("SELECT name from {simplelist}", 0, 1));
    $simplelist_desc = $this->randomName(26, 'simplelist_desc_');
    $web_user = $this->drupalCreateUserRolePerm(array (
      0 => 'access comments',
      1 => 'access content',
      2 => 'administer simplelist',
    ));
    $this->drupalLoginUser($web_user);
    $this->drupalGet(url('admin/build/simplelist', array("absolute" => TRUE)));
    $this->assertWantedRaw('Existing Simplelists', 'Existing Simplelists');
    $this->assertWantedRaw('Administer Simplelists', "Page title");

    $this->clickLink('Add', '0');
    $this->assertWantedRaw('Add a Simplelist', "Page title");
    $test_terms = $this->get_taxonomy_terms(0);
    $edit = array (
      'name' => $simplelist_name,
      'access[1]' => '1',
      'access[2]' => '2',
      'description' => $simplelist_desc,
      'cache' => BLOCK_NO_CACHE,
      'published' => SIMPLELIST_PUBLISHED_NODES,
      'filter_name' => 'SimpleListNodeFilter',
      'node_types[blog]' => 'blog',
      'node_types[image]' => 'image',
      'node_types[page]' => 'page',
      'taxonomy_tids[]' => $test_terms,
      'sort_name' => 'title',
      'sort_data' => 'ASC',
      'block_select' => '1',
      'block_display_name' => 'SimpleListDisplay',
      'block_display_title' => 'Simplelist Block',
      'block_display_count' => '10',
      'block_display_more' => '1',
      'block_display_format' => SIMPLELIST_FORMAT_TEASER,
      'page_select' => '1',
      'page_display_name' => 'SimpleListDisplay',
      'page_display_title' => 'Simplelist Page',
      'page_display_count' => '10',
      'page_display_pager' => '1',
      'page_display_format' => SIMPLELIST_FORMAT_TEASER,
      'page_display_path' => $simplelist_name,
    );
    $this->drupalPost('admin/build/simplelist/add', $edit, 'Save');
    
    $this->assertWantedRaw('Add a Simplelist', '[simplelist] Should be on add page if Save fails.  %s');
    $this->assertWantedRaw('Simplelist names must be unique - please choose a different name, as this one is already in use.',
      '[simplelist] Simplelist names must be unique.');
    
    
  }
  
  function test_name_unique_on_update() {
    $simplelist_duplicate_name = db_result(db_query_range("SELECT name from {simplelist}", 0, 1));
    
    $simplelist_name = $this->randomName(6, 'simplelist_test_');
    $simplelist_desc = $this->randomName(26, 'simplelist_desc_');
    $web_user = $this->drupalCreateUserRolePerm(array (
      0 => 'access comments',
      1 => 'access content',
      2 => 'administer simplelist',
    ));
    $this->drupalLoginUser($web_user);
    $this->drupalGet(url('admin/build/simplelist', array("absolute" => TRUE)));
    $this->assertWantedRaw('Existing Simplelists', 'Existing Simplelists');
    $this->assertWantedRaw('Administer Simplelists', "Page title");

    $this->clickLink('Add', '0');
    $this->assertWantedRaw('Add a Simplelist', "Page title");
    $test_terms = $this->get_taxonomy_terms(0);
    $edit = array (
      'name' => $simplelist_name,
      'access[1]' => '1',
      'access[2]' => '2',
      'description' => $simplelist_desc,
      'cache' => BLOCK_NO_CACHE,
      'published' => SIMPLELIST_PUBLISHED_NODES,
      'filter_name' => 'SimpleListNodeFilter',
      'node_types[blog]' => 'blog',
      'node_types[image]' => 'image',
      'node_types[page]' => 'page',
      'taxonomy_tids[]' => $test_terms,
      'sort_name' => 'title',
      'sort_data' => 'ASC',
      'block_select' => '1',
      'block_display_name' => 'SimpleListDisplay',
      'block_display_title' => 'Simplelist Block',
      'block_display_count' => '10',
      'block_display_more' => '1',
      'block_display_format' => SIMPLELIST_FORMAT_TEASER,
      'page_select' => '1',
      'page_display_name' => 'SimpleListDisplay',
      'page_display_title' => 'Simplelist Page',
      'page_display_count' => '10',
      'page_display_pager' => '1',
      'page_display_format' => SIMPLELIST_FORMAT_TEASER,
      'page_display_path' => $simplelist_name,
    );
    $this->drupalPost('admin/build/simplelist/add', $edit, 'Save');
    $this->assertWantedRaw('Existing Simplelists', 'Existing Simplelists');
    $this->assertWantedRaw('Simplelist '. $simplelist_name .' has been created', '[simplelist] Simplelist was created. %s');
    $this->assertWantedRaw($simplelist_name, '[simplelist] Found name on list of simplelists.');
    $this->assertWantedRaw('Administer Simplelists', "Page title");
    
    $simplelist = simplelist_load($simplelist_name);
    $this->assertNotNull($simplelist, '[simplelist] Should retrieve valid simplelist from database. %s');
    
    $this->assertEqual(array(1, 2), $simplelist->access, '[simplelist] Simplelist has proper access value. %s');
    $this->assertEqual($simplelist_name, $simplelist->name, '[simplelist] Simplelist has proper name. %s');
    $this->assertEqual($simplelist_desc, $simplelist->description, '[simplelist] Simplelist has proper description. %s');
    $this->assertEqual(BLOCK_NO_CACHE, $simplelist->cache, '[simplelist] Simplelist has proper Block Cache. %s');
    $this->assertEqual(SIMPLELIST_PUBLISHED_NODES, $simplelist->published, '[simplelist] Simplelist has proper published value. %s');
    $this->assertEqual('SimpleListNodeFilter', $simplelist->filter_name);
    $this->assertEqual('', $simplelist->filter_data);
    $this->assertEqual(array('blog', 'image', 'page'), $simplelist->node_types);

    $term_tids = array();
    foreach($simplelist->terms as $term) {
      $term_tids[] = $term->tid;
    }
    $this->assertEqual($test_terms, asort($term_tids), '[simpletest] Comparing terms: '. print_r($test_terms, TRUE) .':'. print_r($term_tids, TRUE));
    $this->assertEqual('title', $simplelist->sort_name);
    $this->assertEqual('ASC', $simplelist->sort_data);
    $this->assertTrue(isset($simplelist->displays['block']), '[simplelist] Simplelist has block information.');
    $this->assertEqual('SimpleListDisplay', $simplelist->displays['block']->display_name);
    $this->assertEqual('Simplelist Block', $simplelist->displays['block']->display_title);
    $this->assertEqual(10, $simplelist->displays['block']->display_count);
    $this->assertEqual(1, $simplelist->displays['block']->display_more);
    $this->assertEqual(SIMPLELIST_FORMAT_TEASER, $simplelist->displays['block']->display_format);
    
    $this->assertTrue(isset($simplelist->displays['page']), '[simplelist] Simplelist has page information.');
    $this->assertEqual('SimpleListDisplay', $simplelist->displays['page']->display_name);
    $this->assertEqual('Simplelist Page', $simplelist->displays['page']->display_title);
    $this->assertEqual(10, $simplelist->displays['page']->display_count);
    $this->assertEqual(1, $simplelist->displays['page']->display_pager);
    $this->assertEqual(SIMPLELIST_FORMAT_TEASER, $simplelist->displays['page']->display_format);
    $this->assertEqual($simplelist_name, $simplelist->displays['page']->display_path);
    
    $test_terms = $this->get_taxonomy_terms(1);
    $edit = array (
      'name' => $simplelist_duplicate_name,
      'description' => $simplelist_desc,
      'access[1]' => FALSE,
      'access[2]' => FALSE,
      'cache' => '1',
      'published' => SIMPLELIST_PUBLISHED_NODES,
      'filter_name' => 'SimpleListNodeFilter',
      'node_types[blog]' => 'blog',
      'node_types[image]' => FALSE,
      'node_types[page]' => FALSE,
      'taxonomy_tids[]' => $test_terms,
      'sort_name' => 'created',
      'sort_data' => 'ASC',
      'block_select' => '1',
      'block_display_name' => 'SimpleListDisplay',
      'block_display_title' => 'Simpletest Block',
      'block_display_count' => '5',
      'block_display_more' => '1',
      'block_display_format' => SIMPLELIST_FORMAT_TITLE_LINK,
      'page_select' => '1',
      'page_display_name' => 'SimpleListDisplay',
      'page_display_title' => 'Simplelist Page',
      'page_display_count' => '10',
      'page_display_pager' => '1',
      'page_display_format' => SIMPLELIST_FORMAT_TEASER,
      'page_display_path' => $simplelist_name,
    );
    $this->drupalPost('admin/build/simplelist/'. $simplelist->slid .'/edit', $edit, 'Save');
    
    $this->assertNoUnwantedRaw('user warning:', 'User warning found in page: %s');
    $this->assertWantedRaw('Simplelist names must be unique - please choose a different name, as this one is already in use.',
      '[simplelist] Verify that editing a simplelist to use an existing name fails.  %s');
    $this->assertWantedRaw('Edit a Simplelist', '[simplelist] Should be on edit page if Save fails.  %s');
   

    $edit = array ();
    $this->drupalPost('admin/build/simplelist/delete/'. $simplelist->slid, $edit, 'Delete');
    
    // Need test that item no longer shows here.
    $this->assertWantedRaw('Administer Simplelists', "Page title");
    $this->assertNoUnwantedRaw($simplelist_name, '[simplelist] No longer find simplelist in list.');
    
    $this->assertNull(simplelist_load($simplelist_name), '[simplelist] Cannot get simplelist from database. %s');
  }
}