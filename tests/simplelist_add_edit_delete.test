<?php

/*
 * This class contains tests that try to add, edit, and then delete a simplelist.
 */
class simplelist_add_edit_delete extends DrupalTestCase {
  /**
   * Implementation of get_info() for information
   */
  function get_info() {
    return array('name' => t('Simplelist Add, Edit, and Delete'), 'desc' => 'Create a simplelist, edit it, and then delete it.', 'group' => 'SimpleList');
  }

  function tearDown() {
    parent::tearDown();
  }
  
  function get_taxonomy_terms($start = 0) {
    $query = "SELECT t.tid FROM {term_data} t INNER JOIN {vocabulary} v ON (t.vid = v.vid) ORDER BY t.tid";
    $result = db_query_range($query, $start, 3);
    $return = array();
    while ($term = db_fetch_object($result)) {
      $return[] = $term->tid;
    }
    return $return;
  }
  
  function create_simplelist_for_testing($settings = array()) {
    $defaults = array(
      'name' => $this->randomName(3, 'simplelist_list_'),
      'description' => $this->randomname(26, 'simplelist_desc_'),
      'access' => serialize(array()),
      'filter_name' => 'SimpleListNodeFilter',
      'filter_data' => '',
      'sort_name' => 'created',
      'sort_data' => 'DESC',
      'published' => SIMPLELIST_PUBLISHED_NODES,
      'cache' => BLOCK_NO_CACHE,
    
      'display_name' => 'SimpleListDisplay',
      'display_title' => '',
      'display_context' => 'block',
      'display_count' => 10,
      'display_pager' => 0,
      'display_more' => 0,
      'display_format' => SIMPLELIST_FORMAT_TEASER,
      'display_path' => '',
    );
    
    $simplelist = ($settings + $defaults);
    $simplelist = (object)$simplelist;
    
    drupal_write_record('simplelist', $simplelist);
    drupal_write_record('simplelist_display', $simplelist);
    
    return $simplelist;
  }

  function testsimplelist_add_edit_delete () {
    $simplelist_name = $this->randomName(6, 'simplelist_test_');
    $simplelist_desc = $this->randomName(26, 'simplelist_desc_');
    $web_user = $this->drupalCreateUserRolePerm(array (
      0 => 'access comments',
      1 => 'access content',
      2 => 'administer simplelist',
    ));
    $this->drupalLoginUser($web_user);
    $this->drupalGet(url('admin/build/simplelist', array("absolute" => TRUE)));
    $this->assertWantedRaw('Existing Simplelists', 'Existing Simplelists');
    $this->assertWantedRaw('Administer Simplelists', "Page title");

    $this->clickLink('Add', '0');
    $this->assertWantedRaw('Add a Simplelist', "Page title");
    $test_terms = $this->get_taxonomy_terms(0);
    $edit = array (
      'name' => $simplelist_name,
      'access[1]' => '1',
      'access[2]' => '2',
      'description' => $simplelist_desc,
      'cache' => BLOCK_NO_CACHE,
      'published' => SIMPLELIST_PUBLISHED_NODES,
      'filter_name' => 'SimpleListNodeFilter',
      'node_types[blog]' => 'blog',
      'node_types[image]' => 'image',
      'node_types[page]' => 'page',
      'taxonomy_tids[]' => $test_terms,
      'sort_name' => 'title',
      'sort_data' => 'ASC',
      'block_select' => '1',
      'block_display_name' => 'SimpleListDisplay',
      'block_display_title' => 'Simplelist Block',
      'block_display_count' => '10',
      'block_display_more' => '1',
      'block_display_format' => SIMPLELIST_FORMAT_TEASER,
      'page_select' => '1',
      'page_display_name' => 'SimpleListDisplay',
      'page_display_title' => 'Simplelist Page',
      'page_display_count' => '10',
      'page_display_pager' => '1',
      'page_display_format' => SIMPLELIST_FORMAT_TEASER,
      'page_display_path' => $simplelist_name,
    );
    $this->drupalPost('admin/build/simplelist/add', $edit, 'Save');
    $this->assertWantedRaw('Existing Simplelists', 'Existing Simplelists');
    $this->assertWantedRaw('Simplelist '. $simplelist_name .' has been created', '[simplelist] Simplelist was created. %s');
    $this->assertWantedRaw($simplelist_name, '[simplelist] Found name on list of simplelists.');
    $this->assertWantedRaw('Administer Simplelists', "Page title");
    
    $simplelist = simplelist_load($simplelist_name);
    $this->assertNotNull($simplelist, '[simplelist] Should retrieve valid simplelist from database. %s');
    
    $this->assertEqual(array(1, 2), $simplelist->access, '[simplelist] Simplelist has proper access value. %s');
    $this->assertEqual($simplelist_name, $simplelist->name, '[simplelist] Simplelist has proper name. %s');
    $this->assertEqual($simplelist_desc, $simplelist->description, '[simplelist] Simplelist has proper description. %s');
    $this->assertEqual(BLOCK_NO_CACHE, $simplelist->cache, '[simplelist] Simplelist has proper Block Cache. %s');
    $this->assertEqual(SIMPLELIST_PUBLISHED_NODES, $simplelist->published, '[simplelist] Simplelist has proper published value. %s');
    $this->assertEqual('SimpleListNodeFilter', $simplelist->filter_name);
    $this->assertEqual('', $simplelist->filter_data);
    $this->assertEqual(array('blog', 'image', 'page'), $simplelist->node_types);

    $term_tids = array();
    foreach($simplelist->terms as $term) {
      $term_tids[] = $term->tid;
    }
    $this->assertEqual($test_terms, asort($term_tids));
    $this->assertEqual('title', $simplelist->sort_name);
    $this->assertEqual('ASC', $simplelist->sort_data);
    $this->assertTrue(isset($simplelist->displays['block']), '[simplelist] Simplelist has block information.');
    $this->assertEqual('SimpleListDisplay', $simplelist->displays['block']->display_name);
    $this->assertEqual('Simplelist Block', $simplelist->displays['block']->display_title);
    $this->assertEqual(10, $simplelist->displays['block']->display_count);
    $this->assertEqual(1, $simplelist->displays['block']->display_more);
    $this->assertEqual(SIMPLELIST_FORMAT_TEASER, $simplelist->displays['block']->display_format);
    
    $this->assertTrue(isset($simplelist->displays['page']), '[simplelist] Simplelist has page information.');
    $this->assertEqual('SimpleListDisplay', $simplelist->displays['page']->display_name);
    $this->assertEqual('Simplelist Page', $simplelist->displays['page']->display_title);
    $this->assertEqual(10, $simplelist->displays['page']->display_count);
    $this->assertEqual(1, $simplelist->displays['page']->display_pager);
    $this->assertEqual(SIMPLELIST_FORMAT_TEASER, $simplelist->displays['page']->display_format);
    $this->assertEqual($simplelist_name, $simplelist->displays['page']->display_path);
    
    $test_terms = $this->get_taxonomy_terms(1);
    $edit = array (
      'name' => $simplelist_name,
      'description' => $simplelist_desc,
      'access[1]' => FALSE,
      'access[2]' => FALSE,
      'cache' => '1',
      'published' => SIMPLELIST_PUBLISHED_NODES,
      'filter_name' => 'SimpleListNodeFilter',
      'node_types[blog]' => 'blog',
      'node_types[image]' => FALSE,
      'node_types[page]' => FALSE,
      'taxonomy_tids[]' => $test_terms,
      'sort_name' => 'created',
      'sort_data' => 'ASC',
      'block_select' => '1',
      'block_display_name' => 'SimpleListDisplay',
      'block_display_title' => 'Simpletest Block',
      'block_display_count' => '5',
      'block_display_more' => '1',
      'block_display_format' => SIMPLELIST_FORMAT_TITLE_LINK,
      'page_select' => '1',
      'page_display_name' => 'SimpleListDisplay',
      'page_display_title' => 'Simplelist Page',
      'page_display_count' => '10',
      'page_display_pager' => '1',
      'page_display_format' => SIMPLELIST_FORMAT_TEASER,
      'page_display_path' => $simplelist_name,
    );
    $this->drupalPost('admin/build/simplelist/'. $simplelist->slid .'/edit', $edit, 'Save');
    
    $this->assertNoUnwantedRaw('user warning:', 'User warning found in page: %s');
    $this->assertWantedRaw('Existing Simplelists', 'Existing Simplelists');
    $this->assertWantedRaw('Simplelist '. $simplelist_name .' has been updated', '[simplelist] Found update notice.  %s');
    $this->assertWantedRaw($simplelist_name, '[simplelist] Found name on list of simplelists.');
    $this->assertWantedRaw('Administer Simplelists', "Page title");
    
    unset($simplelist);
    $simplelist = simplelist_load($simplelist_name);
    $this->assertNotNull($simplelist, '[simplelist] Should retrieve valid simplelist from database. %s');
    
    $this->assertEqual(array(), $simplelist->access, '[simplelist] Simplelist has proper access value. %s');
    $this->assertEqual($simplelist_name, $simplelist->name, '[simplelist] Simplelist has proper name. %s');
    $this->assertEqual($simplelist_desc, $simplelist->description, '[simplelist] Simplelist has proper description. %s');
    $this->assertEqual(BLOCK_CACHE_PER_ROLE, $simplelist->cache, '[simplelist] Simplelist has proper Block Cache. %s');
    $this->assertEqual(SIMPLELIST_PUBLISHED_NODES, $simplelist->published, '[simplelist] Simplelist has proper published value. %s');
    $this->assertEqual('SimpleListNodeFilter', $simplelist->filter_name);
    $this->assertEqual('', $simplelist->filter_data);
    $this->assertEqual(array('blog'), $simplelist->node_types);

    $term_tids = array();
    foreach($simplelist->terms as $term) {
      $term_tids[] = $term->tid;
    }
    $this->assertEqual($test_terms, asort($term_tids), 'Comparing '. print_r($simplelist->terms, TRUE));
    $this->assertEqual('created', $simplelist->sort_name);
    $this->assertEqual('ASC', $simplelist->sort_data);
    $this->assertTrue(isset($simplelist->displays['block']), '[simplelist] Simplelist has block information.');
    $this->assertEqual('SimpleListDisplay', $simplelist->displays['block']->display_name);
    $this->assertEqual('Simpletest Block', $simplelist->displays['block']->display_title);
    $this->assertEqual(5, $simplelist->displays['block']->display_count);
    $this->assertEqual(1, $simplelist->displays['block']->display_more);
    $this->assertEqual(SIMPLELIST_FORMAT_TITLE_LINK, $simplelist->displays['block']->display_format);
    
    $this->assertTrue(isset($simplelist->displays['page']), '[simplelist] Simplelist has page information.');
    $this->assertEqual('SimpleListDisplay', $simplelist->displays['page']->display_name);
    $this->assertEqual('Simplelist Page', $simplelist->displays['page']->display_title);
    $this->assertEqual(10, $simplelist->displays['page']->display_count);
    $this->assertEqual(1, $simplelist->displays['page']->display_pager);
    $this->assertEqual(SIMPLELIST_FORMAT_TEASER, $simplelist->displays['page']->display_format);
    $this->assertEqual($simplelist_name, $simplelist->displays['page']->display_path);

    $edit = array ();
    $this->drupalPost('admin/build/simplelist/delete/'. $simplelist->slid, $edit, 'Delete');
    
    // Need test that item no longer shows here.
    $this->assertWantedRaw('Administer Simplelists', "Page title");
    $this->assertNoUnwantedRaw($simplelist_name, '[simplelist] No longer find simplelist in list.');
    
    $this->assertNull(simplelist_load($simplelist_name), '[simplelist] Cannot get simplelist from database. %s');
  }
  
  function testsimplelist_user_role_edit_delete () {
    // First off, create fake simplelist.
    $user_role_data = new stdClass();
    $user_role_data->roles = array();
    $user_role_data->uid = 0;
    $simplelist = $this->create_simplelist_for_testing(array('filter_name' => 'SimpleListFilterNodeByUser', 'filter_data' => serialize($user_role_data)));
    $simplelist_name = $simplelist->name;
    $simplelist_desc = $simplelist->description;
    $slid = $simplelist->slid;
    $this->assertNotEqual(0, $slid);
    
    $web_user = $this->drupalCreateUserRolePerm(array (
      0 => 'access comments',
      1 => 'access content',
      2 => 'administer simplelist',
    ));
    $this->drupalLoginUser($web_user);
    $this->drupalGet(url('admin/build/simplelist', array("absolute" => TRUE)));
    $this->assertWantedRaw('Existing Simplelists', 'Existing Simplelists');
    $this->assertWantedRaw('Administer Simplelists', "Page title");
    $this->assertWantedRaw($simplelist_name, '[simplelist] Assure that stub simplelist has been created! %s');
    
    $edit = array (
      'name' => $simplelist_name,
      'description' => $simplelist_desc,
      'access[1]' => FALSE,
      'access[2]' => FALSE,
      'cache' => '1',
      'published' => SIMPLELIST_PUBLISHED_NODES,
      'filter_name' => 'SimpleListFilterNodeByUser',
      'node_types[blog]' => 'blog',
      'user_roles[1]' => 1,
      'user_name' => $web_user->name,
      'sort_name' => 'created',
      'sort_data' => 'ASC',
      'block_select' => '1',
      'block_display_name' => 'SimpleListDisplay',
      'block_display_title' => 'Simpletest Block',
      'block_display_count' => '5',
      'block_display_more' => '1',
      'block_display_format' => SIMPLELIST_FORMAT_TITLE_LINK,
      'page_select' => '1',
      'page_display_name' => 'SimpleListDisplay',
      'page_display_title' => 'Simplelist Page',
      'page_display_count' => '10',
      'page_display_pager' => '1',
      'page_display_format' => SIMPLELIST_FORMAT_TEASER,
      'page_display_path' => $simplelist_name,
    );
    $this->drupalPost('admin/build/simplelist/'. $simplelist->slid .'/edit', $edit, 'Save');
    
    $this->assertNoUnwantedRaw('user warning:', 'User warning found in page: %s');
    $this->assertWantedRaw('Existing Simplelists', 'Existing Simplelists');
    $this->assertWantedRaw('Simplelist '. $simplelist_name .' has been updated', '[simplelist] Found update notice.  %s');
    $this->assertWantedRaw($simplelist_name, '[simplelist] Found name on list of simplelists.');
    $this->assertWantedRaw('Administer Simplelists', "Page title");
    
    $simplelist = simplelist_load($simplelist_name);
    $this->assertNotNull($simplelist, '[simplelist] Should retrieve valid simplelist from database. %s');
    
    $this->assertEqual(array(), $simplelist->access, '[simplelist] Simplelist has proper access value. %s');
    $this->assertEqual($simplelist_name, $simplelist->name, '[simplelist] Simplelist has proper name. %s');
    $this->assertEqual($simplelist_desc, $simplelist->description, '[simplelist] Simplelist has proper description. %s');
    $this->assertEqual(BLOCK_CACHE_PER_ROLE, $simplelist->cache, '[simplelist] Simplelist has proper Block Cache. %s');
    $this->assertEqual(SIMPLELIST_PUBLISHED_NODES, $simplelist->published, '[simplelist] Simplelist has proper published value. %s');
    $this->assertEqual('SimpleListFilterNodeByUser', $simplelist->filter_name);
    $this->assertEqual(array('blog'), $simplelist->node_types);

    
    $user_role_data = unserialize($simplelist->filter_data);
    $this->assertTrue($user_role_data, "Look, it's ". print_r($user_role_data, TRUE));
    $this->assertNotNull($user_role_data);
    $this->assertEqual(array(1), $user_role_data->roles);
    $this->assertEqual($web_user->uid, $user_role_data->uid);
    
    $this->assertEqual('created', $simplelist->sort_name);
    $this->assertEqual('ASC', $simplelist->sort_data);
    $this->assertTrue(isset($simplelist->displays['block']), '[simplelist] Simplelist has block information.');
    $this->assertEqual('SimpleListDisplay', $simplelist->displays['block']->display_name);
    $this->assertEqual('Simpletest Block', $simplelist->displays['block']->display_title);
    $this->assertEqual(5, $simplelist->displays['block']->display_count);
    $this->assertEqual(1, $simplelist->displays['block']->display_more);
    $this->assertEqual(SIMPLELIST_FORMAT_TITLE_LINK, $simplelist->displays['block']->display_format);
    
    $this->assertTrue(isset($simplelist->displays['page']), '[simplelist] Simplelist has page information.');
    $this->assertEqual('SimpleListDisplay', $simplelist->displays['page']->display_name);
    $this->assertEqual('Simplelist Page', $simplelist->displays['page']->display_title);
    $this->assertEqual(10, $simplelist->displays['page']->display_count);
    $this->assertEqual(1, $simplelist->displays['page']->display_pager);
    $this->assertEqual(SIMPLELIST_FORMAT_TEASER, $simplelist->displays['page']->display_format);
    $this->assertEqual($simplelist_name, $simplelist->displays['page']->display_path);

    $edit = array ();
    $this->drupalPost('admin/build/simplelist/delete/'. $simplelist->slid, $edit, 'Delete');
    
    // Need test that item no longer shows here.
    $this->assertWantedRaw('Administer Simplelists', "Page title");
    $this->assertNoUnwantedRaw($simplelist_name, '[simplelist] No longer find simplelist in list.');
    
    $this->assertNull(simplelist_load($simplelist_name), '[simplelist] Cannot get simplelist from database. %s');
  }

  function testsimplelist_add_edit_delete_cascade_display () {
    $simplelist_name = $this->randomName(6, 'simplelist_test_');
    $simplelist_desc = $this->randomName(26, 'simplelist_desc_');
    $web_user = $this->drupalCreateUserRolePerm(array (
      0 => 'access comments',
      1 => 'access content',
      2 => 'administer simplelist',
    ));
    $this->drupalLoginUser($web_user);
    $this->drupalGet(url('admin/build/simplelist', array("absolute" => TRUE)));
    $this->assertWantedRaw('Existing Simplelists', 'Existing Simplelists');
    $this->assertWantedRaw('Administer Simplelists', "Page title");

    $this->clickLink('Add', '0');
    $this->assertWantedRaw('Add a Simplelist', "Page title");
    $test_terms = $this->get_taxonomy_terms(1);
    $edit = array (
      'name' => $simplelist_name,
      'access[1]' => '1',
      'access[2]' => '2',
      'description' => $simplelist_desc,
      'cache' => BLOCK_NO_CACHE,
      'published' => SIMPLELIST_PUBLISHED_NODES,
      'filter_name' => 'SimpleListNodeFilter',
      'node_types[blog]' => 'blog',
      'node_types[image]' => 'image',
      'node_types[page]' => 'page',
      'taxonomy_tids[]' => $test_terms,
      'sort_name' => 'title',
      'sort_data' => 'ASC',
      'block_select' => '1',
      'block_display_name' => 'SimpleListDisplay',
      'block_display_title' => 'Simplelist Block',
      'block_display_count' => '10',
      'block_display_more' => '1',
      'block_display_format' => SIMPLELIST_FORMAT_TEASER,
      'page_select' => '1',
      'page_display_name' => 'SimpleListDisplayCascade',
      'page_display_title' => 'Simplelist Page',
      'page_display_count' => '10',
      'page_display_pager' => '1',
      'page_display_format' => SIMPLELIST_FORMAT_FULL,
      'page_display_path' => $simplelist_name,
    );
    $this->drupalPost('admin/build/simplelist/add', $edit, 'Save');
    $this->assertWantedRaw('Existing Simplelists', 'Existing Simplelists');
    $this->assertWantedRaw('Simplelist '. $simplelist_name .' has been created', '[simplelist] Simplelist was created. %s');
    $this->assertWantedRaw($simplelist_name, '[simplelist] Found name on list of simplelists.');
    $this->assertWantedRaw('Administer Simplelists', "Page title");
    
    $simplelist = simplelist_load($simplelist_name);
    $this->assertNotNull($simplelist, '[simplelist] Should retrieve valid simplelist from database. %s');
    
    $this->assertEqual(array(1, 2), $simplelist->access, '[simplelist] Simplelist has proper access value. %s');
    $this->assertEqual($simplelist_name, $simplelist->name, '[simplelist] Simplelist has proper name. %s');
    $this->assertEqual($simplelist_desc, $simplelist->description, '[simplelist] Simplelist has proper description. %s');
    $this->assertEqual(BLOCK_NO_CACHE, $simplelist->cache, '[simplelist] Simplelist has proper Block Cache. %s');
    $this->assertEqual(SIMPLELIST_PUBLISHED_NODES, $simplelist->published, '[simplelist] Simplelist has proper published value. %s');
    $this->assertEqual('SimpleListNodeFilter', $simplelist->filter_name);
    $this->assertEqual('', $simplelist->filter_data);
    $this->assertEqual(array('blog', 'image', 'page'), $simplelist->node_types);

    $term_tids = array();
    foreach($simplelist->terms as $term) {
      $term_tids[] = $term->tid;
    }
    $this->assertEqual($test_terms, asort($term_tids));
    $this->assertEqual('title', $simplelist->sort_name);
    $this->assertEqual('ASC', $simplelist->sort_data);
    $this->assertTrue(isset($simplelist->displays['block']), '[simplelist] Simplelist has block information.');
    $this->assertEqual('SimpleListDisplay', $simplelist->displays['block']->display_name);
    $this->assertEqual('Simplelist Block', $simplelist->displays['block']->display_title);
    $this->assertEqual(10, $simplelist->displays['block']->display_count);
    $this->assertEqual(1, $simplelist->displays['block']->display_more);
    $this->assertEqual(SIMPLELIST_FORMAT_TEASER, $simplelist->displays['block']->display_format);
    
    $this->assertTrue(isset($simplelist->displays['page']), '[simplelist] Simplelist has page information.');
    $this->assertEqual('SimpleListDisplayCascade', $simplelist->displays['page']->display_name);
    $this->assertEqual('Simplelist Page', $simplelist->displays['page']->display_title);
    $this->assertEqual(10, $simplelist->displays['page']->display_count);
    $this->assertEqual(1, $simplelist->displays['page']->display_pager);
    $this->assertEqual(SIMPLELIST_FORMAT_FULL, $simplelist->displays['page']->display_format);
    $this->assertEqual($simplelist_name, $simplelist->displays['page']->display_path);
    
    $test_terms = $this->get_taxonomy_terms(0);
    $edit = array (
      'name' => $simplelist_name,
      'description' => $simplelist_desc,
      'access[1]' => FALSE,
      'access[2]' => FALSE,
      'cache' => '1',
      'published' => SIMPLELIST_PUBLISHED_NODES,
      'filter_name' => 'SimpleListNodeFilter',
      'node_types[blog]' => 'blog',
      'node_types[image]' => FALSE,
      'node_types[page]' => FALSE,
      'taxonomy_tids[]' => $test_terms,
      'sort_name' => 'created',
      'sort_data' => 'ASC',
      'block_select' => '1',
      'block_display_name' => 'SimpleListDisplayCascade',
      'block_display_title' => 'Simpletest Block',
      'block_display_count' => '5',
      'block_display_more' => FALSE,
      'block_display_format' => SIMPLELIST_FORMAT_TITLE_LINK,
      'page_select' => '1',
      'page_display_name' => 'SimpleListDisplay',
      'page_display_title' => 'Simplelist Page',
      'page_display_count' => '20',
      'page_display_pager' => FALSE,
      'page_display_format' => SIMPLELIST_FORMAT_TEASER,
      'page_display_path' => $simplelist_name,
    );
    $this->drupalPost('admin/build/simplelist/'. $simplelist->slid .'/edit', $edit, 'Save');
    
    $this->assertNoUnwantedRaw('user warning:', 'User warning found in page: %s');
    $this->assertWantedRaw('Existing Simplelists', 'Existing Simplelists');
    $this->assertWantedRaw('Simplelist '. $simplelist_name .' has been updated', '[simplelist] Found update notice.  %s');
    $this->assertWantedRaw($simplelist_name, '[simplelist] Found name on list of simplelists.');
    $this->assertWantedRaw('Administer Simplelists', "Page title");
    
    unset($simplelist);
    $simplelist = simplelist_load($simplelist_name);
    $this->assertNotNull($simplelist, '[simplelist] Should retrieve valid simplelist from database. %s');
    
    $this->assertEqual(array(), $simplelist->access, '[simplelist] Simplelist has proper access value. %s');
    $this->assertEqual($simplelist_name, $simplelist->name, '[simplelist] Simplelist has proper name. %s');
    $this->assertEqual($simplelist_desc, $simplelist->description, '[simplelist] Simplelist has proper description. %s');
    $this->assertEqual(BLOCK_CACHE_PER_ROLE, $simplelist->cache, '[simplelist] Simplelist has proper Block Cache. %s');
    $this->assertEqual(SIMPLELIST_PUBLISHED_NODES, $simplelist->published, '[simplelist] Simplelist has proper published value. %s');
    $this->assertEqual('SimpleListNodeFilter', $simplelist->filter_name);
    $this->assertEqual('', $simplelist->filter_data);
    $this->assertEqual(array('blog'), $simplelist->node_types);

    $term_tids = array();
    foreach($simplelist->terms as $term) {
      $term_tids[] = $term->tid;
    }
    $this->assertEqual($test_terms, asort($term_tids), 'Comparing '. print_r($simplelist->terms, TRUE));
    $this->assertEqual('created', $simplelist->sort_name);
    $this->assertEqual('ASC', $simplelist->sort_data);
    $this->assertTrue(isset($simplelist->displays['block']), '[simplelist] Simplelist has block information.');
    $this->assertEqual('SimpleListDisplayCascade', $simplelist->displays['block']->display_name);
    $this->assertEqual('Simpletest Block', $simplelist->displays['block']->display_title);
    $this->assertEqual(5, $simplelist->displays['block']->display_count);
    $this->assertEqual(0, $simplelist->displays['block']->display_more);
    $this->assertEqual(SIMPLELIST_FORMAT_TITLE_LINK, $simplelist->displays['block']->display_format);
    
    $this->assertTrue(isset($simplelist->displays['page']), '[simplelist] Simplelist has page information.');
    $this->assertEqual('SimpleListDisplay', $simplelist->displays['page']->display_name);
    $this->assertEqual('Simplelist Page', $simplelist->displays['page']->display_title);
    $this->assertEqual(20, $simplelist->displays['page']->display_count);
    $this->assertEqual(0, $simplelist->displays['page']->display_pager);
    $this->assertEqual(SIMPLELIST_FORMAT_TEASER, $simplelist->displays['page']->display_format);
    $this->assertEqual($simplelist_name, $simplelist->displays['page']->display_path);

    $edit = array ();
    $this->drupalPost('admin/build/simplelist/delete/'. $simplelist->slid, $edit, 'Delete');
    
    // Need test that item no longer shows here.
    $this->assertWantedRaw('Administer Simplelists', "Page title");
    $this->assertNoUnwantedRaw($simplelist_name, '[simplelist] No longer find simplelist in list.');
    
    $this->assertNull(simplelist_load($simplelist_name), '[simplelist] Cannot get simplelist from database. %s');
  }
};
