<?php

require_once(drupal_get_path('module', 'simplelist') .'/SimpleListDisplayParent.php');

/**
 * This display class takes a list of nodes, and presents them to the user in series.
 *
 */
class SimpleListDisplayCascade extends SimpleListDisplayParent {
  
  /**
   * The render function takes an array of nodes and presents them in series based on the simplelist data.
   *
   * @param stdClass $simple_list
   *   Simplelist
   * @param array $node_array
   *   Array of node objects
   * @return string
   *   HTML to present
   */
  public function render($simple_list, $node_array) {
    $items = array();
    
    foreach($node_array as $node) {
      if ($simple_list->display->display_format == SIMPLELIST_FORMAT_TITLE_LINK) {
        $items[] = l($node->title, 'node/'. $node->nid);
      }
      else if ($simple_list->display->display_format == SIMPLELIST_FORMAT_THEME) {
        $items[] = theme(array("simplelist__$simple_list->name", "simplelist"), $node, $simple_list);
      }
      else if ($simple_list->display->display_format == SIMPLELIST_FORMAT_TEASER) {
        if (isset($node->teaser) && isset($node->body)) {
          $items[] = node_view($node, TRUE, FALSE, TRUE);
        }
        else {
          $items[] = theme('node', $node, TRUE, FALSE);
        }
      }
      else if ($simple_list->display->display_format == SIMPLELIST_FORMAT_FULL) {
        if (isset($node->teaser) || isset($node->body)) {
          $items[] = node_view($node, FALSE, FALSE, TRUE);
        }
        else {
          $items[] = theme('node', $node, FALSE, FALSE);
        }
      }
    }
    $result_nodes = implode("\n", $items);
    if ($simple_list->display->display_pager == 1) {
      $result_nodes .= theme('pager', array(), $simple_list->display->display_count);
    }
    if ($simple_list->display->display_more_path != '') {
      $result_nodes .= l('More', $simple_list->display->display_more_path);
    }
    return $result_nodes;
  }
  
  public static function get_display_form($simplelist, $format='block') {
    $form = array();
    if ($format == 'block') {
      $form['block_display_title'] = array(
        '#type' => 'textfield',
        '#title' => t('Block Title'),
        '#default_value' => $simplelist->displays['block']->display_title,
        '#size' => 80,
        '#maxlength' => 255,
        '#weight' => -5,
      );
      
      $form['block_display_count'] = array(
        '#type' => 'textfield',
        '#title' => t('Block Item Count'),
        '#default_value' => $simplelist->displays['block']->display_count,
        '#size' => 6,
        '#maxlength' => 3,
        '#weight' => -3,
      );
      
      $form['block_display_more'] = array(
        '#type' => 'checkbox',
        '#title' => t('Display \'More\' link - requires page view.'),
        '#default_value' => $simplelist->displays['block']->display_more,
        '#weight' => -1,
      );
      
      $form['block_display_format'] = array(
        '#type' => 'select',
        '#title' => t('Node Format'),
        '#default_value' => $simplelist->displays['block']->display_format,
        '#options' => array(
          SIMPLELIST_FORMAT_TEASER => t('As Teaser'),
          SIMPLELIST_FORMAT_FULL => t('As Full Node'),
          SIMPLELIST_FORMAT_THEME => t('As Themed'),
          SIMPLELIST_FORMAT_TITLE_LINK => t('As Titles, linking to node'),
        ),
        '#multiple' => FALSE,
        '#required' => TRUE,
        '#description' => t('Choose how to display the nodes.  If you use \'As Themed\', you should define a theme_simplelist__$name($node, $simplelist) function, where $name is the name of this simplelist.'),
        '#weight' => 1,
      );
      
      $form['block_display_path'] = array(
        '#type' => 'hidden',
        '#title' => t('Argument Path'),
        '#default_value' => $simplelist->displays['block']->display_path,
        '#size' => 80,
        '#maxlength' => 255,
        '#weight' => 3,
      );
    }
    else if ($format == 'page') {
      $form['page_display_title'] = array(
        '#type' => 'textfield',
        '#title' => t('Page Title'),
        '#default_value' => $simplelist->displays['page']->display_title,
        '#size' => 80,
        '#maxlength' => 255,
        '#weight' => -7,
      );
      
      $form['page_display_count'] = array(
        '#type' => 'textfield',
        '#title' => t('Page Item Count'),
        '#default_value' => $simplelist->displays['page']->display_count,
        '#size' => 6,
        '#maxlength' => 3,
        '#weight' => -5,
      );
      
      $form['page_display_pager'] = array(
        '#type' => 'checkbox',
        '#title' => t('Display pager at bottom of page?'),
        '#default_value' => $simplelist->displays['page']->display_pager,
        '#weight' => -3,
      );
      
      $form['page_display_format'] = array(
        '#type' => 'select',
        '#title' => t('Node Format'),
        '#default_value' => $simplelist->displays['page']->display_format,
        '#options' => array(
          SIMPLELIST_FORMAT_TEASER => t('As Teaser'),
          SIMPLELIST_FORMAT_FULL => t('As Full Node'),
          SIMPLELIST_FORMAT_THEME => t('As Themed'),
          SIMPLELIST_FORMAT_TITLE_LINK => t('As Titles, linking to node'),
        ),
        '#multiple' => FALSE,
        '#required' => TRUE,
        '#description' => t('Choose how to display the nodes.  If you use \'As Themed\', you should define a theme_simplelist__$name($node, $simplelist) function, where $name is the name of this simplelist.'),
        '#weight' => -1,
      );
      
      $form['page_display_path'] = array(
        '#type' => 'textfield',
        '#title' => t('Page Path'),
        '#default_value' => $simplelist->displays['page']->display_path,
        '#size' => 80,
        '#maxlength' => 255,
        '#weight' => 1,
        '#description' => t('Enter the path to display this page at.  For arguments you can include %tid to include the termid, or %node_type to include the node type.  If these are included, they\'ll override whatever term or type were assigned as a parameter.  I don\'t recommend using a parameter for the first part of the path.'),
      );
    }
    return $form;
  }
  
  public static function clear_existing_settings($slid, $form_id='', &$form_state=NULL, $format='block') {
    // There's no existing settings that won't be overritten anyway.

  }
}
