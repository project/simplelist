SIMPLETEST INSTALL INSTRUCTIONS:

Untar/unzip, and copy the resulting simplelist directory into sites/all/modules.  Then go to the admin/build/modules page 
to enable it as usual.

But note well: This module was written for PHP 5 - it won't work in PHP 4.  And if your install of PHP has trouble with the 'static'
keyword, it will have trouble with this - some early versions of PHP 5 seem to have a problem with that sort of thing.
