<?php
require_once(drupal_get_path('module', 'simplelist') .'/SimpleListFilterParent.php');

class SimpleListNodeQueueFilter extends SimpleListFilterParent {
  
  public function get_node_list($simple_list, $count, $offset, $paged) {
    $nodes = array();
    
    $query = "SELECT n.nid FROM {node} n INNER JOIN {nodequeue_nodes} nqn "
    ."ON (nqn.nid = n.nid AND nqn.qid = %d)";
    $query_args = array($simple_list->filter_data);
    
    $where = '';
    $where_args = array();
    
    if ($simple_list->published == SIMPLELIST_PUBLISHED_NODES || $simple_list->published == SIMPLELIST_UNPUBLISHED_NODES) {
      if ($where != '') {
        $where .= " AND";
      }
      else {
        $where .= " WHERE";
      }
      $where .= " n.status = %d";
      $where_args[] = $simple_list->published;
    }
    
    if ($simple_list->sort_data == 'ASC') {
      $order = ' ORDER BY nqn.position ASC';
    }
    else {
      $order = ' ORDER BY nqn.position DESC';
    }

    $arguments = array_merge($query_args, $where_args);
    if ($paged) {
      $results = pager_query(db_rewrite_sql($query . $where . $order), $count, 0, NULL, $arguments);
    }
    else {
      $results = db_query_range(db_rewrite_sql($query . $where . $order), $arguments, $offset, $count);
    }
    
    while ($node_id = db_fetch_object($results)) {
      $nodes[] = $this->cache_engine->fetch_node($node_id->nid);
    }
    
    return $nodes;
  }
  
  public static function get_filter_form($simplelist) {
    $form = array();
    
    $form['simplelist_nodequeue_select'] = array(
      '#type' => 'select',
      '#options' => SimpleListNodeQueueFilter::get_nodequeues(),
      '#default_value' => $simplelist->filter_data,
      '#required' => TRUE,
      '#multiple' => FALSE,
      '#title' => t('Nodequeue'),
      '#description' => t('Select the nodequeue to read nodes from.'),
    );

    $form['sort_data'] = array(
      '#type' => 'radios',
      '#title' => t('Nodequeue Sort Direction'),
      '#default_value' => $simplelist->sort_data,
      '#options' => array('ASC' => t('Ascending'), 'DESC' => t('Descending')),
      '#weight' => 2,
    );
    
    return $form;   
  }
  public static function get_filter_form_validate(&$form, &$form_state) {
    // No real validation - it's just a dropdown and a sort order
  }
  
  public static function get_filter_form_submit($form_id, &$form_state) {
    $nodequeue = $form_state['values']['simplelist_nodequeue_select'];
    $sort_data = $form_state['values']['sort_data'];
    $slid = $form_state['values']['slid'];
    
    $query = "UPDATE {simplelist} SET filter_data = %d AND sort_name='nodequeue' AND sort_data = '%s' WHERE slid = %d";
    db_query($query, $nodequeue, $sort_data, $slid);
    
  }
  
  public static function clear_existing_settings($slid, $form_id='', &$form_state=NULL) {
    $query = "UPDATE {simplelist} SET filter_data = '' WHERE slid = %d";
    db_query($query, $form_state['values']['slid']);
  }
  
  public static function get_nodequeues() {
    $queues = array();
    $sql = 'SELECT nq.qid, nq.title '.
       'FROM {nodequeue_queue} nq '.
       'WHERE nq.show_in_ui = 1 ';
    $results = db_query($sql);
    while($row = db_fetch_object($results)) {
      $queues[$row->qid] = $row->title;
    }
    return $queues;
  }
}